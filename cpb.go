package main

import (
	a "codonPairBias/algorithms"
	u "codonPairBias/utilities"
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"runtime/pprof"
	"strings"
	"time"
)

const (
	minText       = "Seek the minimum total bias"
	timeText      = "Run each test for the specified maximum time"
	inputText     = "Use specified gene file"
	outputText    = "Output solution to specified file"
	boundText     = "Use specified value as a bound on the total bias"
	pairBiasText  = "Use specified pair bias file"
	synText       = "Use specified synonymous codon file"
	nText         = "Consider only the specified number of codons at the beginning of the file"
	dirText       = "Base directory in which to look for files"
	algorithmText = "Algorithm to use for optimizing."
	rText         = "Number of times to run the heuristic."
	fText         = "Which function code should perform"
	exText        = "How many codons should extend consider at a time"
	randText      = "Start from random solution for simulated annealing"
)

var seekMinDef = true
var maxTimeDef = time.Second * 60
var inputNameDef = "seq.fasta"
var outputNameDef = "out.fasta"
var boundDef = u.Inf
var pairBiasNameDef = "bias.txt"
var synNameDef = "syn_codons.txt"
var nDef = 20
var dirDef = "../../../../"
var algorithmDef = "NaiveDynamic"
var rDef = 1
var exDef = 50
var randDef = true

var cpuprofile = ""

func main() {

	var seekMin bool
	var maxTime time.Duration
	var inputName string
	var outputName string
	var bound float64
	var pairBiasName string
	var synName string
	var n int
	var dir string
	var algorithm string
	var r int
	var extendSectionSize int
	var randomSim bool

	flag.StringVar(&cpuprofile, "cpuprofile", "", "write cpu profile to file")
	flag.BoolVar(&seekMin, "minimize", seekMinDef, minText)
	flag.DurationVar(&maxTime, "time", maxTimeDef, timeText)
	flag.StringVar(&inputName, "input", inputNameDef, inputText)
	flag.StringVar(&outputName, "output", outputNameDef, outputText)
	flag.Float64Var(&bound, "bound", boundDef, boundText)
	flag.StringVar(&pairBiasName, "pairBias", pairBiasNameDef, pairBiasText)
	flag.StringVar(&synName, "syn", synNameDef, synText)
	flag.IntVar(&n, "number", nDef, nText)
	flag.StringVar(&dir, "dir", dirDef, dirText)
	flag.StringVar(&algorithm, "algorithm", algorithmDef, algorithmText)
	flag.IntVar(&r, "run", rDef, rText)
	flag.IntVar(&extendSectionSize, "extendSectionSize", exDef, exText)

	flag.BoolVar(&seekMin, "m", seekMinDef, minText)
	flag.DurationVar(&maxTime, "t", maxTimeDef, timeText)
	flag.StringVar(&inputName, "i", inputNameDef, inputText)
	flag.StringVar(&outputName, "o", outputNameDef, outputText)
	flag.Float64Var(&bound, "b", boundDef, boundText)
	flag.StringVar(&pairBiasName, "p", pairBiasNameDef, pairBiasText)
	flag.StringVar(&synName, "s", synNameDef, synText)
	flag.IntVar(&n, "n", nDef, nText)
	flag.StringVar(&dir, "d", dirDef, dirText)
	flag.StringVar(&algorithm, "a", algorithmDef, algorithmText)
	flag.IntVar(&r, "r", rDef, rText)
	flag.IntVar(&extendSectionSize, "e", exDef, exText)
	flag.BoolVar(&randomSim, "z", randDef, randText)

	flag.Parse()
	if randomSim {
		fmt.Printf("random\n")
	}
	if cpuprofile != "" {
		f, err := os.Create(cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}
	//End CLI
	//
	// Fix paths
	if dir != "" {
		if path.Base(pairBiasName) == pairBiasName {
			pairBiasName = path.Join(dir, pairBiasName)
		}
		if path.Base(synName) == synName {
			synName = path.Join(dir, synName)
		}
		if path.Base(inputName) == inputName {
			inputName = path.Join(dir, inputName)
		}
		if path.Base(outputName) == outputName {
			outputName = path.Join(dir, outputName)
		}
	}

	//Open and read files
	bias, err := u.ReadBiasFile(pairBiasName)
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	if !seekMin {
		u.NegateBiases(bias)
		bound *= -1.0 //TODO: Change NegateBiases to NegateProblemInstance?
	}
	seq, err := u.ReadGeneFile(inputName, n)
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	codonToAmino, aminoToCodon, err := u.ReadSynonymousFile(synName)
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	//End read files
	//
	//Choose algorithm
	alg, ok := a.StringToAlgorithm[strings.ToLower(algorithm)]
	if !ok {
		fmt.Printf("%s Not Found\n", algorithm)
		alg = a.StringToAlgorithm[strings.ToLower(algorithmDef)]
	}
	pi, err := a.MakeProblemInstance(aminoToCodon, bias, bound, codonToAmino, n, seekMin, seq, maxTime)

	if extendSectionSize > 1 { //Deal with extendSectionSize
		pi.ExtendSectionSize = extendSectionSize
	} else {
		fmt.Printf("Problem using extendSectionSize %d. Using default value %d instead.", extendSectionSize, exDef)
		pi.ExtendSectionSize = exDef
	}
	if err != nil {
		fmt.Println(err.Error())
		panic(err)
	}
	oldSeq := make([]byte, pi.N)
	copy(oldSeq, pi.Seq)
	ps, err := alg(pi)
	if err != nil {
		fmt.Println(err.Error())
		fmt.Println("Halted")
	} else {
		copy(pi.Seq, oldSeq)
		err = a.CheckSolution(pi, ps)
		if err != nil {
			fmt.Println(err.Error())
			for _, codNum := range pi.Seq {
				codonStr, _ := u.CodonString(codNum)
				fmt.Printf("%s ", codonStr)
			}
			fmt.Printf("\n\n")
		}
		for _, codNum := range ps.Solution {
			codonStr, _ := u.CodonString(codNum)
			fmt.Printf("%s ", codonStr)
		}
		fmt.Printf("\n")
		/*for _, codNum := range ps.Solution {
			fmt.Printf("%2d  ", codNum)
		}
		fmt.Printf("\n")*/
		fmt.Printf("Score:\n%f\nTime:\n%f\n", ps.Score, ps.T.Seconds())
		//fmt.Println(pi.Options)
	}

}
