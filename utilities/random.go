//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"math/rand"
	"time"
)

var random = rand.New(rand.NewSource(time.Now().UnixNano()))

func RFloat64() float64 {
	return random.Float64()
}

func RInt() int {
	return random.Int()
}

func RIntn(n int) int {
	return random.Intn(n)
}

func RPerm(n int) []int {
	return random.Perm(n)
}

func RSeed(seed int64) {
	random.Seed(seed)
}
