//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"fmt"
	"testing"
)

func TestReadBiasFile(t *testing.T) {
	_, err := ReadBiasFile("../../../../bias.txt")
	if err != nil {
		t.Error(err)
	}

}

func TestNegateBiases(t *testing.T) {
	bias1, err := ReadBiasFile("../../../../bias.txt")
	if err != nil {
		t.Error(err)
	}
	bias2, err := ReadBiasFile("../../../../bias.txt")
	if err != nil {
		t.Error(err)
	}
	NegateBiases(bias1)
	for i, biasRow := range bias1 {
		for j, _ := range biasRow {
			if bias1[i][j] != -1.0*bias2[i][j] {
				t.Error(fmt.Sprintf("Bias %v negated to %v rather than to negate to %v.", bias2[i][j], bias1[i][j], -1.0*bias2[i][j]))
			}
		}
	}

}

func TestNormalizeBiases(t *testing.T) {
	bias1, err := ReadBiasFile("../../../../bias.txt")
	if err != nil {
		t.Error(err)
	}
	bias2, err := ReadBiasFile("../../../../bias.txt")
	if err != nil {
		t.Error(err)
	}
	offset := NormalizeBiases(bias1)
	for i, biasRow := range bias1 {
		for j, _ := range biasRow {
			if !NearlyEquals(bias1[i][j], bias2[i][j] - offset) {
				t.Error(fmt.Sprintf("Bias %v  normalized to %v not to %v.", bias2[i][j], bias1[i][j], bias2[i][j] - offset))
			}
			if bias1[i][j] < 0.0 {
				t.Error(fmt.Sprintf("Bias %v normalized to %v less than 0.", bias2[i][j], bias1[i][j]))
			}
		}
	}

}
