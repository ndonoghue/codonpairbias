//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"errors"
	"fmt"
)

//OptionsSlice takes a byte slice corresponding to codons, a slice for codon number to string
//and a map from string to slice of bytes. It returns a 2d slice corresponding to codon options
//indexed first by location.
func CountSlice(seq []byte) ([]int, error) {
	counts := make([]int, 64)
	for i, cod := range seq {
		if cod < 0 || cod > 63 {
			return nil, errors.New(fmt.Sprintf("CountSlice: Invalid codon number %v at position %v", cod, i))
		}
		counts[cod] += 1
	}
	return counts, nil
}
