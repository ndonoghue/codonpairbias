//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

/*
var MaxTillCheck = 2000000

type ProblemInstance struct {
	AminoToCodon map[string][]byte
	Bias         [][]float64
	Bound        float64
	CodonToAmino []string
	Count        []int
	N            int
	Offset       float64
	Options      [][]byte
	SeekMin      bool
	Seq          []byte
	T            time.Duration
	maxTillCheck int
}

type ProblemSolution struct {
	Score           float64
	Solution        []byte
	T               time.Duration
	tillCheck       int
	fixed           bool
	start           time.Time
	LastChosenIndex int
}

type DynamicNode struct {
	Count          [64]uint8
	Index          int
	LastCodonIndex byte
	Score          float64
	sliceIndex     int
	Parent         *DynamicNode
}

type DynamicNodeMap struct {
	maps  []map[[64]uint8]*DynamicNode
	Slice []*DynamicNode
}

//ConditionalUpdate updates the map to include the passed DynamicNode iff the passed DynamicNode
//is the best yet seen with the same Count array.
func (m *DynamicNodeMap) ConditionalUpdate(dn *DynamicNode) {
	dno, exists := m.maps[dn.LastCodonIndex][dn.Count]
	if exists {
		if dno.Score > dn.Score { //Forget dno, he lost
			dno.LastCodonIndex = dn.LastCodonIndex
			dno.Score = dn.Score
			dno.Parent = dn.Parent //The rest of the fields apply to both
			return                 //dno, true
		} else { //Forget dn, he lost
			return //dno, false
		}
	} else {
		dn.sliceIndex = len(m.Slice)
		m.Slice = append(m.Slice, dn)
		m.maps[dn.LastCodonIndex][dn.Count] = m.Slice[dn.sliceIndex]
		return //dn, true
	}
}

func MakeDynamicNodeMap(index int, pi *ProblemInstance) *DynamicNodeMap {
	dnm := new(DynamicNodeMap)
	dnm.maps = make([]map[[64]uint8]*DynamicNode, len(pi.Options[index]))
	for i := range dnm.maps {
		dnm.maps[i] = make(map[[64]uint8]*DynamicNode)
	}
	dnm.Slice = make([]*DynamicNode, 0)
	return dnm
}

//MakeProblemInstance creates a new ProblemInstance
func MakeProblemInstance(aminoToCodon map[string][]byte, bias [][]float64, bound float64, codonToAmino []string,
	n int, seekMin bool, seq []byte, t time.Duration) (*ProblemInstance, error) {
	var err error
	pi := new(ProblemInstance)
	pi.AminoToCodon = aminoToCodon
	pi.Bias = bias
	pi.Bound = bound
	pi.CodonToAmino = codonToAmino
	pi.N = n
	pi.SeekMin = seekMin
	pi.T = t
	if len(seq) < n {
		return pi, errors.New(fmt.Sprintf("Specified sequence only %d long, less than %d", len(seq), n))
	}
	pi.Seq = seq[:n]
	pi.Options, err = OptionsSlice(aminoToCodon, codonToAmino, pi.Seq)
	if err != nil {
		return pi, err
	}
	pi.Count, err = CountSlice(pi.Seq)
	if err != nil {
		return pi, err
	}
	pi.maxTillCheck = MaxTillCheck
	ps, err := MakeProblemSolution(pi, true)
	if err != nil {
		return pi, err
	}
	copy(ps.Solution, pi.Seq)
	err = ScoreSolution(bias, ps)
	if err != nil {
		return pi, err
	}
	pi.Bound = math.Min(pi.Bound, ps.Score)

	return pi, nil
}

//MakeProblemInstance creates a new ProblemInstance
func MakeProblemSolution(pi *ProblemInstance, isOpt bool) (*ProblemSolution, error) {
	ps := new(ProblemSolution)
	if isOpt {
		ps.Score = pi.Bound
		ps.Solution = make([]byte, pi.N)
	} else {
		ps.Score = 0.0
		ps.Solution = make([]byte, 0, pi.N)
	}
	ps.T = 0
	ps.start = time.Now()
	ps.tillCheck = 0
	ps.fixed = false
	return ps, nil

}

//FixProblemSolution deal with the offset and negates the score based on seekMin
func FixProblemSolution(pi *ProblemInstance, ps *ProblemSolution) {
	if ps.fixed {
		return
	}
	ps.Score = ps.Score + float64(pi.N-1)*pi.Offset
	if !pi.SeekMin {
		ps.Score *= -1.0
	}
	ps.fixed = true
	ps.T = time.Since(ps.start)
}

//OutOfTime decides whether a partial solution is out of time
func OutOfTime(pi *ProblemInstance, ps *ProblemSolution) bool {
	if ps.tillCheck == pi.maxTillCheck {
		ps.T = time.Since(ps.start)
		if ps.T > pi.T {
			return true
		}
		ps.tillCheck = 0
	} else {
		ps.tillCheck += 1
	}
	return false
}
*/
