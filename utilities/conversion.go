//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"bytes"
)

// CodonNumber returns the codon number of a specified string.
func CodonNumber(codon string) (byte, bool) {
	var ret byte
	ret = 0
	ok := len(codon) == 3
	for i := 0; ok && i < 3; i++ {
		ret *= 4
		switch c := codon[i]; c {
		case 'g', 'G':
		case 'c', 'C':
			ret += 1
		case 'a', 'A':
			ret += 2
		case 't', 'T', 'u', 'U':
			ret += 3
		default:
			ok = false
		}
	}
	if !ok {
		ret = 65
	}
	return ret, ok
}

//CodonString returns the codon string associated with a specified codon number.
func CodonString(codonNumber byte) (string, bool) {
	var buffer [3]byte
	ok := 0 <= codonNumber && codonNumber < 64
	for i := 0; i < 3 && ok; i++ {
		switch rem := codonNumber % 4; rem {
		case 0:
			buffer[2-i] = 'G'
		case 1:
			buffer[2-i] = 'C'
		case 2:
			buffer[2-i] = 'A'
		case 3:
			buffer[2-i] = 'T'
		}
		codonNumber /= 4
	}
	ret := string(buffer[:])
	if !ok {
		ret = ""
	}
	return ret, ok
}

//CanonicalCodonString Returns the canonical (for this package) string representation of a specified codon string  (i.e.,
// the 3 character string containing only 'G', 'C', 'A', or 'T'
func CanonicalCodonString(codonString string) (string, bool) {
	var buffer bytes.Buffer
	ok := len(codonString) == 3
	for i := 0; i < 3 && ok; i++ {
		switch c := codonString[i]; c {
		case 'G', 'g':
			buffer.WriteString("G")
		case 'C', 'c':
			buffer.WriteString("C")
		case 'A', 'a':
			buffer.WriteString("A")
		case 'T', 't', 'U', 'u':
			buffer.WriteString("T")
		default:
			ok = false
		}
	}
	ret := buffer.String()
	if !ok {
		ret = ""
	}
	return ret, ok
}
