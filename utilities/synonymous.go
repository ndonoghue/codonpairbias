//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strings"
)

//ReadSynonymousFile takes a string specifiying the location of a synonymous codon file
//and returns a 64-array of strings mapping codon number to amino acid, a map from amino acid strings
//to slice of codon numbers, and an error. The synonymous file is
//expected to be of the form:
//SynonymousList
//SynonymousList -> SynonymousLine [SynonymousList]
//SynonymousLine -> CodonList AminoLabel
//CodonList -> Codon [CodonList]
//
//where each AminoLabel and each Codon appear on only one line.
//
func ReadSynonymousFile(synFileName string) ([]string, map[string][]byte, error) {
	codonToAmino := make([]string, 64)
	aminoToCodons := make(map[string][]byte)
	file, err := os.Open(synFileName)
	if err != nil {
		return codonToAmino, nil, err
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fields := strings.Fields(scanner.Text())
		amino := fields[len(fields)-1]
		fields = fields[:len(fields)-1]
		codons := make([]byte, len(fields), len(fields))
		for index, codonString := range fields {
			codonNum, ok := CodonNumber(codonString)
			if !ok {
				return codonToAmino, nil, errors.New(fmt.Sprintf("Invalid codon identifier \"%s\" in"+
					"\"%s\".", codonString, synFileName))
			}
			if _, ok = aminoToCodons[amino]; ok {
				return codonToAmino, nil, errors.New(fmt.Sprintf("Invalid synonymous file \"%s\" with duplicated"+
					"amino identifier \"%s\".", synFileName, amino))
			}
			codonToAmino[codonNum] = amino
			codons[index] = codonNum
		}
		aminoToCodons[amino] = codons
	}
	return codonToAmino, aminoToCodons, nil

}
