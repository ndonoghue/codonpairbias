//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
)

//ReadBiasFile takes a string specifiying the location of a bias file
//and returns a 64x64 array of biases where the array is indexed by left codon
//then by right codon.
//The bias file is
//expected to contain 64*64 lines, each of the form
//CodonLeft CodonRight Bias
//
//where each ordered pair of codons occurs exactly once.
//
func ReadBiasFile(biasFileName string) ([][]float64, error) {
	biasArr := make([]float64, 64*64)
	bias := make([][]float64, 64)
	for index, _ := range bias {
		bias[index], biasArr = biasArr[:64], biasArr[64:]
	}
	file, err := os.Open(biasFileName)
	defer file.Close()
	if err != nil {
		return bias, err
	}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		fields := strings.Fields(line)
		if len(fields) != 3 {
			return bias, errors.New(fmt.Sprintf("Invalid line \"%s\" in \"%s\".", line, biasFileName))
		}
		codL, ok := CodonNumber(fields[0])
		if !ok {
			return bias, errors.New(fmt.Sprintf("Invalid codon identifier \"%s\" in"+
				"\"%s\".", fields[0], biasFileName))
		}

		codR, ok := CodonNumber(fields[1])
		if !ok {
			return bias, errors.New(fmt.Sprintf("Invalid codon identifier \"%s\" in"+
				"\"%s\".", fields[1], biasFileName))
		}
		codBias, err := strconv.ParseFloat(fields[2], 64)
		bias[codL][codR] = codBias
		if err != nil {
			return bias, err
		}

	}
	return bias, err
}

//NegateBiases takes a 2d slice of biases and negates each cell
func NegateBiases(bias [][]float64) {
	for i, biasRow := range bias {
		for j, _ := range biasRow {
			bias[i][j] = -1.0 * bias[i][j]
		}
	}
}
