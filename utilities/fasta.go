//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"os"
)

//CodonToAminoSlice takes a string specifying a genome fasta file and returns a slice of bytes
//indicating by position which codon occurred
func ReadGeneFile(fastaFileName string, n int) ([]byte, error) {
	file, err := os.Open(fastaFileName)
	defer file.Close()
	if err != nil {
		return nil, err
	}
	scanner := bufio.NewScanner(file)
	ret := make([]byte, 0, 100)
	ok := scanner.Scan()
	if !ok {
		return nil, errors.New(fmt.Sprintf("Invalid FASTA file \"%s\". Cannot read first line.", fastaFileName))
	}
	line := scanner.Text()
	if line[0] != '>' {
		return nil, errors.New(fmt.Sprintf("Invalid FASTA file \"%s\". Does not begin with \">\".", fastaFileName))
	}
	var buffer bytes.Buffer // Used for leftover at end of line
	for scanner.Scan() {    //This loop reads through the remaining lines in the file
		line = scanner.Text()
		codonStr := ""
		var codonNum byte
		var ok bool
		if bufLen := buffer.Len(); bufLen != 0 { //If there was leftover

			little := readN(line, 3-bufLen)
			buffer.WriteString(little)
			if buffer.Len() != 3 {
				//fmt.Println(buffer.String())
				continue
			}
			codonStr = buffer.String()
			buffer.Reset()
			//fmt.Println(buffer.String())
			codonNum, ok = CodonNumber(codonStr)
			if !ok {
				return ret, errors.New(fmt.Sprintf("Invalid codon identifier \"%s\".", codonStr))
			}
			ret = append(ret, codonNum)
			line = line[3-bufLen:] //Remove whatever was used to finish leftover
		}
		for i := 0; i < len(line); i += 3 { // This loop iterates by three, reading 3 chars, converting to codon number and appending
			codonStr = readN(line[i:], 3)
			if len(codonStr) != 3 {
				buffer.WriteString(codonStr)
				break
			}
			codonNum, ok = CodonNumber(codonStr)
			if !ok {
				return ret, errors.New(fmt.Sprintf("Invalid codon identifier \"%s\".", codonStr))
			}
			ret = append(ret, codonNum)
			if len(ret) >= n {
				return ret[:n], nil
			}
		}
	}
	if bufLen := buffer.Len(); bufLen != 0 {
		return ret, errors.New(fmt.Sprintf("File \"%s\" does not contain a number of bases divisible by 3.", fastaFileName))
	}
	if len(ret) < n {
		return ret, errors.New(fmt.Sprintf("File \"%s\" contains only %d codons rather than the "+
			"requested %d codons.", fastaFileName, len(ret), n))
	}
	return ret, nil
}

func readN(str string, n int) string {
	if len(str) < n {
		return str
	} else {
		return str[:n]
	}
}
