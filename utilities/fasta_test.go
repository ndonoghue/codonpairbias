//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"fmt"
	"testing"
)

func TestReadGeneFile(t *testing.T) {
	codonSlice, err := ReadGeneFile("../../../../hemoglobin.fasta", 100)
	if err != nil {
		t.Error(err)
	}
	for _, codonNum := range codonSlice {
		if codonNum < 0 || codonNum > 63 {
			t.Error(fmt.Sprintf("Invalid Codon byte returned"))
		}
	}
}
