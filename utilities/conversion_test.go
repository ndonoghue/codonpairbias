//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"fmt"
	"strings"
	"testing"
)

func TestCodonString(t *testing.T) {
	var i byte
	for i = 0; i < 64; i++ {
		str, ok := CodonString(i)
		if !ok {
			t.Error(fmt.Sprintf("Error produced for valid codon identifier %d", i))
		} else if len(str) != 3 {
			t.Error(fmt.Sprintf("Invalid codon identifier \"%s\" produced for valid codon identifier %d", str, i))
		}
	}
	for i = 64; ; {
		str, ok := CodonString(i)
		if ok {
			t.Error(fmt.Sprintf("Error not produced for invalid codon identifier %d", i))
		}
		if str != "" {
			t.Error(fmt.Sprintf("String \"%s\" produced for invalid codon identifier %d", str, i))
		}
		i++
		if i == 0 { //This is necesarry to catch the 255 case
			break
		}
	}
	str, ok := CodonString(0)
	if !ok {
		t.Error(fmt.Sprintf("Error produced for valid codon identifier %d", 0))
	}
	if str != "GGG" {
		t.Error(fmt.Sprintf("\"%s\" not \"GGG\" produced for codon identifier 0", str))
	}
	str, ok = CodonString(63)
	if !ok {
		t.Error(fmt.Sprintf("Error produced for valid codon identifier %d", 0))
	}
	if str != "TTT" {
		t.Error(fmt.Sprintf("\"%s\" not \"TTT\" produced for codon identifier 63", str))
	}
	str, ok = CodonString(21)
	if !ok {
		t.Error(fmt.Sprintf("Error produced for valid codon identifier %d", 0))
	}
	if str != "CCC" {
		t.Error(fmt.Sprintf("\"%s\" not \"CCC\" produced for codon identifier 21", str))
	}

}

func TestCodonStringBackToNumber(t *testing.T) {
	var i byte
	for i = 0; i < 64; i++ {
		str, ok := CodonString(i)
		if !ok {
			t.Error(fmt.Sprintf("Error produced for valid codon identifier %d", i))
		} else {
			j, ok := CodonNumber(str)
			if !ok {
				t.Error(fmt.Sprintf("Error produced for valid codon identifier string \"%s\"", str))
			} else if j != i {
				t.Error(fmt.Sprintf("Codon identifier %d produced \"%s\" which in turn produced %d", i, str, j))
			}
		}
	}
}

func TestCanonicalCodonString(t *testing.T) {
	var i byte
	for i = 0; i < 64; i++ {
		str, ok := CodonString(i)
		if !ok {
			t.Error(fmt.Sprintf("Error produced for valid codon identifier %d", i))
		}
		strLower := strings.ToLower(str)
		strCanonical, ok := CanonicalCodonString(strLower)
		if !ok {
			t.Error(fmt.Sprintf("Error produced for valid codon identifier \"%s\"", strLower))
		} else if strCanonical != str {
			t.Error(fmt.Sprintf("Canonical codon string for \"%s\" is \"%s\" which does not match \"%s\"", strLower, strCanonical, str))
		}
	}
}
