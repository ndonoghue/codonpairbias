//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import "math"

//The minimum positive normalized value representable by float64
var MinNormal float64 = math.Exp2(-1022)
var Inf float64 = math.Inf(1)
var Ninf float64 = math.Inf(-1)
var MaxFloat float64 = math.MaxFloat64
var epsilon float64 = 0.000000000001

//NearlyEqualsEpsilon takes a, b, and epsilon, all float64, and returns whether a is nearly equal to b with respect to epsilon
func NearlyEqualsEpsilon(a float64, b float64, epsilon float64) bool {
	absA := math.Abs(a)
	absB := math.Abs(b)
	diff := math.Abs(a - b)

	if a == b { // shortcut, handles infinities
		return true
	} else if a == 0 || b == 0 || diff < MinNormal {
		// a or b is zero or both are extremely close to it
		// relative error is less meaningful here
		return diff < (epsilon * MinNormal)
	} else { // use relative error
		return diff/math.Min((absA+absB), MaxFloat) < epsilon
	}
}

//NearlyEquals takes a, b (float64) and returns if they're nearly equal wrt. to standardized epsilon
func NearlyEquals(a float64, b float64) bool {
	/*if NearlyEqualsEpsilon(a, b, epsilon) {
		fmt.Printf("%f %f nearly equals\n", a, b)
	}*/
	return NearlyEqualsEpsilon(a, b, epsilon)
}

func Exp(exponent float64) float64 {
	return math.Exp(exponent)
}
