//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"errors"
	"fmt"
)

//OptionsSlice takes a byte slice corresponding to codons, a slice for codon number to string
//and a map from string to slice of bytes. It returns a 2d slice corresponding to codon options
//indexed first by location.
func OptionsSlice(aminoToCodon map[string][]byte, codonToAmino []string, seq []byte) ([][]byte, error) {
	opts := make([][]byte, len(seq))
	for i, cod := range seq {
		if cod < 0 || cod > 63 {
			return nil, errors.New(fmt.Sprintf("OptionsSlice: Invalid codon number %v at position %v", cod, i))
		}
		cods, ok := aminoToCodon[codonToAmino[cod]]
		if !ok {
			return nil, errors.New(fmt.Sprintf("OptionsSlice: Invalid amino acid identifier %s for codon number %v", codonToAmino[cod], cod))
		}
		opts[i] = make([]byte, len(cods))
		copy(opts[i], cods)
	}
	return opts, nil
}
