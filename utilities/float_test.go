//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"fmt"
	"testing"
)

func TestNearlyEquals(t *testing.T) {
	var a float64
	var b float64
	a = 0.000000000001000001
	b = 0.000000000001000002
	ok := NearlyEquals(a, b)
	if !ok {
		t.Error(fmt.Sprintf("%f and %f not nearly equals", a, b))
	}
}
