//Package utilities provides utiltity functions for working with codons, amino acids, and fasta
package utilities

import (
	"fmt"
	"testing"
)

func TestReadSynFile(t *testing.T) {
	codonToAmino, aminoToCodons, err := ReadSynonymousFile("../../../../syn_codons.txt")
	if err != nil {
		t.Error(err)
	}
	var i byte
	for i = 0; i < 64; i++ {
		amino := codonToAmino[i]
		codons := aminoToCodons[amino]
		var foundIt bool
		for _, cod := range codons {
			if cod == i {
				foundIt = true
			}
		}
		if !foundIt {
			t.Error(fmt.Sprintf("Codon %v not found in the codon list of the amino acid \"%s\".", i, amino))
		}
	}
}
