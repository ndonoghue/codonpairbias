package algorithms

import "fmt"

func Wildtype(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("Wildtype")
	ps, err := MakeProblemSolution(pi, false)
	ps.Solution = pi.Seq
	if err != nil {
		return ps, err
	}
	ScoreSolution(pi.Bias, ps)
	FixProblemSolution(pi, ps)
	return ps, nil
}
