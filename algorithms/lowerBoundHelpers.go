//codonPairBias/algorithms contains several  algorithms for optimizing codon pair bias while holding global codon bias constant.
package algorithms

import (
	u "codonPairBias/utilities"
	"math"
	"sort"
)

func viterbiProblemInstance(pi *ProblemInstance) ([][]float64, error) {
	usable := makeUsableBitVector(pi.Count)
	vit, err := viterbi(usable, pi)
	return vit, err
}

func viterbi(usable usableBitVector, pi *ProblemInstance) ([][]float64, error) {
	vit := make([][]float64, len(pi.Options))
	for i, slotOpts := range pi.Options { //Make 2d slice
		vit[i] = make([]float64, len(slotOpts))
	}
	for i, codR := range pi.Options[len(pi.Options)-1] { //Initialize last column
		if usable.isUsable(codR) { //if codR usable
			vit[len(pi.Options)-1][i] = 0.0
		} else {
			vit[len(pi.Options)-1][i] = u.Inf
		}
	}
	for i := len(pi.Options) - 2; i >= 0; i-- { //start at penultimate column
		for j, codL := range pi.Options[i] {
			if !usable.isUsable(codL) && i != 0 { // codL not usable
				continue
			}
			min := u.Inf
			for k, codR := range pi.Options[i+1] {
				if !usable.isUsable(codR) { //codR not usable
					continue
				}
				min = math.Min(min, vit[i+1][k]+pi.Bias[codL][codR]) // cost from i+1, k cell of vit plus bias
			}
			vit[i][j] = min //min from inner for loop
		}
	}
	return vit, nil
}

func greedySetup(pi *ProblemInstance) (map[string][]*GreedyNode, [][][]*GreedyNode) {
	aminoToGreedy := make(map[string][]*GreedyNode)
	for _, amino := range pi.Aminos {
		aSlice := make([]*GreedyNode, 0)
		codons := pi.AminoToCodon[amino]
		for index, codon := range pi.Seq {
			if pi.CodonToAmino[codon] != amino {
				continue
			}
			for _, codonM := range codons {
				if pi.Count[codonM] <= 0 {
					continue
				}
				if index == 0 && index == pi.N-1 {
					g := new(GreedyNode)
					g.Codon = codonM
					g.Position = index
					g.Score = 0.0
					aSlice = append(aSlice, g)
				} else if index == 0 {
					for _, codonR := range pi.Options[index+1] { //iterate over codon poss. for rNegihbor
						if pi.Count[codonR] <= 0 {
							continue
						}
						g := new(GreedyNode)
						g.Codon = codonM
						g.CodonRight = codonR
						g.Position = index
						g.Score = pi.Bias[codonM][codonR] / 2.0
						aSlice = append(aSlice, g)
					}
				} else if index == pi.N-1 {
					for _, codonL := range pi.Options[index-1] { //it. over codon poss. for lNeighbor
						if pi.Count[codonL] <= 0 {
							continue
						}
						g := new(GreedyNode)
						g.Codon = codonM
						g.CodonLeft = codonL
						g.Position = index
						g.Score = pi.Bias[codonL][codonM] / 2.0
						aSlice = append(aSlice, g)
					}
				} else {
					for _, codonL := range pi.Options[index-1] { //it. over codon poss. for lNeighbor
						if pi.Count[codonL] <= 0 {
							continue
						}
						for _, codonR := range pi.Options[index+1] { //iterate over codon poss. for rNegihbor
							if pi.Count[codonR] <= 0 {
								continue
							}
							g := new(GreedyNode)
							g.Codon = codonM
							g.CodonLeft = codonL
							g.CodonRight = codonR
							g.Position = index
							g.Score = (pi.Bias[codonL][codonM] + pi.Bias[codonM][codonR]) / 2.0
							aSlice = append(aSlice, g)
						}
					}

				}
			}

		}
		sort.Sort(GNByScore(aSlice))
		aminoToGreedy[amino] = aSlice
	}

	edgeNodes := make([][][]*GreedyNode, pi.N)
	for index, cods := range pi.Options {
		edgeNodes[index] = make([][]*GreedyNode, len(cods))
		for codIndex, codon := range cods {
			if pi.Count[codon] <= 0 {
				continue
			}
			if index == pi.N-1 { //No need for edge
				continue
			}
			if index == pi.N-2 { //Can't build full pair
				slc := make([]*GreedyNode, 0)
				for _, codNext := range pi.Options[index+1] {
					if pi.Count[codNext] <= 0 {
						continue
					}
					g := new(GreedyNode)
					g.CodonLeft = codon
					g.Codon = codNext
					g.Score = pi.Bias[codon][codNext]
					g.Position = index + 1
					slc = append(slc, g)
				}
				sort.Sort(GNByScore(slc))
				edgeNodes[index][codIndex] = slc
				continue
			}
			slc := make([]*GreedyNode, 0)
			for _, codNext := range pi.Options[index+1] {
				if pi.Count[codNext] <= 0 {
					continue
				}
				for _, codNextNext := range pi.Options[index+2] {
					if pi.Count[codNextNext] <= 0 {
						continue
					}
					g := new(GreedyNode)
					g.CodonLeft = codon
					g.Codon = codNext
					g.CodonRight = codNextNext
					g.Position = index + 1
					g.Score = pi.Bias[codon][codNext] + (pi.Bias[codNext][codNextNext] / 2.0) //TODO:CHECK
					slc = append(slc, g)
				}
			}
			sort.Sort(GNByScore(slc))
			edgeNodes[index][codIndex] = slc
		}
	}

	return aminoToGreedy, edgeNodes
}

func scoreGreedy(pi *ProblemInstance, count []uint8, index int, lastCodonIndex byte,
	aminoToGreedy map[string][]*GreedyNode, edgeNodes [][][]*GreedyNode) float64 { //TODO:Remove useless gns, don't bother decrementing toPlace
	var score float64
	if index == pi.N-1 { //Can't place anymore
		return score
	}
	countDyn := make([]uint8, 64)
	copy(countDyn, count)
	//countChanges := make([]uint8, 64)
	toPlace := (pi.N - index) - 1
	gns := make([]*GreedyNode, toPlace)
	isEnd := true
	if index < pi.N-2 {
		isEnd = false
	}
	if index >= 0 {
		for _, gPos := range edgeNodes[index][lastCodonIndex] { //Place edge
			if count[gPos.Codon] <= 0 { // >= uint8(pi.Count[gPos.Codon]) { //Codon exhausted
				continue
			}
			if !isEnd && count[gPos.CodonRight] <= 0 { // >= uint8(pi.Count[gPos.CodonRight])) { //Actually using CodonRight, and it's exhausted
				continue
			}
			gns[0] = gPos
			toPlace--
			score += gPos.Score
			break
		}
	}
	//	newScore := 0.0
	if isEnd {
		return score
	}
	for _, amino := range pi.Aminos {
		aminoPlaced := 0
		aminoToPlace := 0
		for _, amino := range pi.Aminos {
			for _, codon := range pi.Seq[index+2:] {
				if amino != pi.CodonToAmino[codon] {
					continue
				}
				aminoToPlace += 1
			}
		}
		for _, gPos := range aminoToGreedy[amino] {
			if aminoPlaced >= aminoToPlace {
				break
			}
			if gPos.Position <= index+1 { //already placed by search
				continue
			}
			if gns[gPos.Position-(index+1)] != nil { //already placed by greedy
				continue
			}
			if count[gPos.CodonLeft] <= 0 || countDyn[gPos.Codon] <= 0 { //using exhausted codons
				continue
			}
			if gPos.Position != pi.N-1 && count[gPos.CodonRight] <= 0 { // >= uint8(pi.Count[gPos.CodonRight]) { //using CodonRight and it's exhausted
				continue
			}
			gns[gPos.Position-(index+1)] = gPos
			toPlace--
			aminoPlaced++
			score += gPos.Score
			if gPos.Position == 0 || gPos.Position == pi.N-1 {
				continue
			}
			countDyn[gPos.Codon]--

		}
		copy(countDyn, count)
		/*for _, codon := range pi.AminoToCodon[amino] {
			countDyn[codon] += countChanges[codon]
		}*/
	}
	/*fmt.Println(pi.Options)
	fmt.Println(pi.Bias[44][14] + 0.5*(pi.Bias[14][28]))
	fmt.Println(pi.Seq)
	fmt.Println(gns)
	gf := make([]*GreedyNode, len(gns))
	g := new(GreedyNode)
	g.CodonLeft = pi.Seq[index]
	g.Codon = pi.Seq[index+1]
	g.CodonRight = pi.Seq[index+2]
	g.Score = pi.Bias[g.CodonLeft][g.Codon] + (pi.Bias[g.Codon][g.CodonRight] / 2.0)
	gf[0] = g
	for i, cod := range pi.Seq[index+1:] {
		if i == 0 || i >= pi.N-1 {
			continue
		}
		g = new(GreedyNode)
		g.CodonLeft = pi.Seq[i-1]
		g.Codon = cod
		g.CodonRight = pi.Seq[i+1]
		g.Score = (pi.Bias[g.CodonLeft][g.Codon] + pi.Bias[g.Codon][g.CodonRight]) / 2.0
		fmt.Println("i ", i)
		gf[i] = g
	}

	g = new(GreedyNode)
	g.CodonLeft = pi.Seq[pi.N-2]
	g.Codon = pi.Seq[pi.N-1]
	g.Score = pi.Bias[g.CodonLeft][g.Codon] / 2.0
	gf[len(gf)-1] = g
	var testScore float64
	fmt.Printf("lengns %d index %d\n", len(gns), index)
	fmt.Println(gf)
	fmt.Println("gf")
	if true {
		return score
	}
	for i, g := range gns {
		fmt.Printf("%v gre\n", g)
		fmt.Printf("%v seq\n", gf[i])
		newScore += g.Score
		fmt.Println("i index", i, index)

		testScore += gf[i].Score
	}
	fmt.Println(testScore)
	fmt.Println(newScore)

	for j, g := range gns[:len(gns)-1] {
		fmt.Printf("seq bias %f\n", pi.Bias[pi.Seq[index+1+j]][pi.Seq[index+1+j+1]])
		fmt.Printf("gre bias %f\n", pi.Bias[g.CodonLeft][g.Codon])
		fmt.Printf("g   bias %f\n", g.Score-pi.Bias[g.Codon][g.CodonRight])
	}
	fmt.Printf("%v %d\n", newScore == score, len(gns))*/
	return score
}
