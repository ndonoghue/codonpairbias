package algorithms

import (
	u "codonPairBias/utilities"
	"fmt"
)

func ExtendGreedy(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("ExtendGreedy")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}

	err = helperExtend(pi, optSol, 0)

	oldSeq := make([]byte, pi.N)
	copy(oldSeq, pi.Seq)
	//prep pi
	copy(pi.Seq, optSol.Solution)
	optSol, err = GreedyHeuristic(pi)
	copy(pi.Seq, oldSeq)
	//FixProblemSolution(pi, optSol) is already done by greedy. Don't do it again
	return optSol, err

	//return nil, nil
}

func Extend(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("Extend")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}

	err = helperExtend(pi, optSol, 0)
	if err != nil {
		return optSol, err
	}
	//prep pi
	FixProblemSolution(pi, optSol)
	//FixProblemSolution(pi, optSol) is already done by greedy. Don't do it again
	return optSol, err

	//return nil, nil
}

func ExtendSA(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("ExtendSA")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}

	err = helperExtend(pi, optSol, 0)

	//prep pi
	oldSeq := make([]byte, pi.N)
	copy(oldSeq, pi.Seq)
	copy(pi.Seq, optSol.Solution)
	pi.SimSettings.afterExtend = true
	optSol, err = SimulatedAnnealing(pi)
	copy(pi.Seq, oldSeq)
	//FixProblemSolution(pi, optSol) is already done by greedy. Don't do it again
	return optSol, err

	//return nil, nil
}

func helperExtend(pi *ProblemInstance, optSol *ProblemSolution, lookback int) error {
	n := pi.N
	bigSol := make([]byte, n)
	for miniStart := 0; miniStart < n; miniStart += pi.ExtendSectionSize { //iterate through chunks
		miniN := u.Min(pi.ExtendSectionSize, n-miniStart)
		if miniStart != 0 { //need to look back one slot
			miniStart--
			miniN++
		}
		miniSeq := make([]byte, miniN)

		for i := 0; i < miniN; i++ {
			miniSeq[i] = pi.Seq[miniStart+i]
		}
		if miniStart != 0 { //need to look back one slot
			miniSeq[0] = bigSol[miniStart] // copy over the last slot
		}
		miniSeqCopy := make([]byte, miniN)
		copy(miniSeqCopy, miniSeq)
		miniPi, err := MakeProblemInstance(pi.AminoToCodon, pi.Bias, u.Inf, pi.CodonToAmino, miniN,
			pi.SeekMin, miniSeqCopy, pi.T)
		if err != nil { //TODO: Handle better
			return err
		}
		if miniStart != 0 { //need to make sure only one option for the fake 0th slot
			miniPi.Options[0] = make([]byte, 1)
			miniPi.Options[0][0] = miniSeq[0]
		}
		if miniN > 1 {
			miniSeqSafe := make([]byte, pi.N-1)
			copy(miniSeqSafe, miniSeq[1:])
			miniPiSafe, err := MakeProblemInstance(pi.AminoToCodon, pi.Bias, u.Inf, pi.CodonToAmino, miniN-1,
				pi.SeekMin, miniSeqSafe, pi.T)

			greedySol, err := GreedyHeuristic(miniPiSafe)
			if err == nil {
				miniPi.Bound = greedySol.Score + miniPi.Bias[miniPi.Seq[0]][greedySol.Solution[0]]
			}
		}
		miniOptSol, err := MakeProblemSolution(miniPi, true)
		if err != nil { //TODO: Handle better
			return err
		}
		vit, err := viterbi(miniPi.Usable, miniPi)
		if err != nil {
			return err
		}
		vc := makeViterbiCache(miniPi.MaxCache)
		noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, dn *DynamicNode) bool {
			if dn.Index == probInst.N-1 {
				return false
			}
			if dn.Score > optSolu.Score && !u.NearlyEquals(dn.Score, optSolu.Score) {
				return true
			}
			lastIndex := dn.Index
			/*if lastIndex == -1 {
				min := u.Inf
				for _, f := range vit[0] {
					min = math.Min(min, f)
				}
				score := dn.Score + min
				return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
			}*/
			score := dn.Score + vit[lastIndex][dn.LastCodonIndex]
			if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
				return true //fmt.Printf("%f score %f optScore\n", score, optSolu.Score)
			}
			dynVit, _ := vc.GetViterbi(dn.Usable, probInst)
			score = dn.Score + dynVit[lastIndex][dn.LastCodonIndex]
			if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
				return true
			}

			return false
		}

		err = helperDynamic(miniPi, miniOptSol, noHope)
		if err != nil { //TODO: Handle better
			return err
		}
		//fmt.Println("miniScore ", miniOptSol.Score)

		if miniStart != 0 {
			for i := 0; i < miniN; i++ { //skip 0th since this is a fake
				bigSol[miniStart+i] = miniOptSol.Solution[i]
			}
			miniStart++
		} else {
			for i := 0; i < miniN; i++ { // don't skip 0th index since it's not fake
				bigSol[i] = miniOptSol.Solution[i]
			}
		}

	}
	optSol.Solution = bigSol
	err := ScoreSolution(pi.Bias, optSol)
	fmt.Println("Big Score ", optSol.Score)
	if err != nil { //TODO: handle better
		return err
	}

	return nil
}
