//codonPairBias/algorithms contains several  algorithms for optimizing codon pair bias while holding global codon bias constant.
package algorithms

import (
	u "codonPairBias/utilities"
	"errors"
	"fmt"
	"math"
	"time"
)

var MaxTillCheck = 2000 //000000

const maxCacheSize = 1600000000 //16000000000

type solutionEvaluator func(*ProblemInstance, *ProblemSolution, *ProblemSolution) bool

type dynamicNodeEvaluator func(*ProblemInstance, *ProblemSolution, *DynamicNode) bool

type aNodelowerBound func(*ProblemInstance, *ANode) float64

type Algorithm func(*ProblemInstance) (*ProblemSolution, error)

type usableBitVector uint64

var usableMask usableBitVector

func init() {
	usableMask = 1
}

func (usable usableBitVector) exhaust(codon uint8) usableBitVector {
	usable |= (usableMask << codon)  //make sure bit set on
	usable &^= (usableMask << codon) //toggle bit
	return usable
}

func (usable usableBitVector) replenish(codon uint8) usableBitVector {
	usable |= (usableMask << codon) //set bit on
	return usable
}

func (usable usableBitVector) isUsable(codon uint8) bool {
	usable &= (usableMask << codon) //isolate bit
	return usable > 0               //if bit set this will be true
}

func makeUsableBitVector(count []int) usableBitVector {
	var usable usableBitVector
	usable = 0x0000000000000000
	var ui uint8
	for i, codCount := range count {
		ui = uint8(i)
		if codCount > 0 {
			usable = usable.replenish(ui)
		}
	}
	return usable
}

type SimulatedAnnealingSettings struct {
	afterExtend bool
}

type ProblemInstance struct {
	AminoToCodon      map[string][]byte
	Aminos            []string
	Bias              [][]float64
	Bound             float64
	CodonToAmino      []string
	Count             []int
	UCount            [64]uint8
	N                 int
	Offset            float64
	Options           [][]byte
	SeekMin           bool
	Seq               []byte
	T                 time.Duration
	maxTillCheck      int
	Usable            usableBitVector
	MaxCache          int
	ExtendSectionSize int
	SimSettings       SimulatedAnnealingSettings
}

type ProblemSolution struct {
	Score           float64
	Solution        []byte
	T               time.Duration
	tillCheck       int
	fixed           bool
	start           time.Time
	LastChosenIndex int
}

type ReconstructNode struct {
	Parent *ReconstructNode
	Choice uint8
}

type DynamicNode struct {
	Count          [64]uint8
	Index          int
	LastCodonIndex byte
	Score          float64
	sliceIndex     int
	//Parent         *DynamicNode
	Usable      usableBitVector
	Reconstruct *ReconstructNode
}

func (r *ReconstructNode) ReconstructSequence(seq []uint8) error {
	return r.reconstructSequence(seq, len(seq)-1)
}

func (r *ReconstructNode) reconstructSequence(seq []uint8, index int) error {
	seq[index] = r.Choice
	if index == 0 {
		return nil
	}
	if r.Parent == nil {
		return errors.New(fmt.Sprintf("Error Reconstructing Sequence at index %d", index))
	}
	return r.Parent.reconstructSequence(seq, index-1)
}

type DynamicNodeMap struct {
	maps  []map[[64]uint8]*DynamicNode
	Slice []*DynamicNode
}

//ConditionalUpdate updates the map to include the passed DynamicNode iff the passed DynamicNode
//is the best yet seen with the same Count array.
func (m *DynamicNodeMap) ConditionalUpdate(dn *DynamicNode) {
	dno, exists := m.maps[dn.LastCodonIndex][dn.Count]
	if exists {
		if dno.Score > dn.Score { //Forget dno, he lost
			dno.LastCodonIndex = dn.LastCodonIndex
			dno.Score = dn.Score
			//dno.Parent = dn.Parent //The rest of the fields apply to both
			dno.Reconstruct = dn.Reconstruct
			return //dno, true
		} else { //Forget dn, he lost
			return //dno, false
		}
	} else {
		dn.sliceIndex = len(m.Slice)
		m.Slice = append(m.Slice, dn)
		m.maps[dn.LastCodonIndex][dn.Count] = m.Slice[dn.sliceIndex]
		return //dn, true
	}
}

func MakeDynamicNodeMap(index int, pi *ProblemInstance) *DynamicNodeMap {
	dnm := new(DynamicNodeMap)
	dnm.maps = make([]map[[64]uint8]*DynamicNode, len(pi.Options[index]))
	for i := range dnm.maps {
		dnm.maps[i] = make(map[[64]uint8]*DynamicNode)
	}
	dnm.Slice = make([]*DynamicNode, 0)
	return dnm
}

//MakeProblemInstance creates a new ProblemInstance
func MakeProblemInstance(aminoToCodon map[string][]byte, bias [][]float64, bound float64, codonToAmino []string,
	n int, seekMin bool, seq []byte, t time.Duration) (*ProblemInstance, error) {
	var err error
	pi := new(ProblemInstance)
	pi.AminoToCodon = aminoToCodon
	pi.Aminos = make([]string, 0)
	for k, _ := range pi.AminoToCodon {
		pi.Aminos = append(pi.Aminos, k)
	}
	pi.Bias = bias
	pi.Bound = bound
	pi.CodonToAmino = codonToAmino
	pi.N = n
	pi.SeekMin = seekMin
	pi.T = t
	if len(seq) < n {
		return pi, errors.New(fmt.Sprintf("Specified sequence only %d long, less than %d", len(seq), n))
	}
	pi.Seq = make([]byte, n)
	copy(pi.Seq, seq)
	//pi.Seq = seq[:n]
	pi.Options, err = u.OptionsSlice(aminoToCodon, codonToAmino, pi.Seq)
	if err != nil {
		return pi, err
	}
	pi.Count, err = u.CountSlice(pi.Seq)
	for codon, codonCount := range pi.Count {
		pi.UCount[codon] = uint8(codonCount)
	}
	if err != nil {
		return pi, err
	}
	pi.maxTillCheck = MaxTillCheck
	ps, err := MakeProblemSolution(pi, true)
	if err != nil {
		return pi, err
	}
	copy(ps.Solution, pi.Seq)
	err = ScoreSolution(bias, ps)
	if err != nil {
		return pi, err
	}
	pi.Bound = math.Min(pi.Bound, ps.Score)
	pi.Usable = makeUsableBitVector(pi.Count)
	pi.MaxCache = int(float64(maxCacheSize) / float64(256*pi.N))
	return pi, nil
}

//MakeProblemInstance creates a new ProblemInstance
func MakeProblemSolution(pi *ProblemInstance, isOpt bool) (*ProblemSolution, error) {
	ps := new(ProblemSolution)
	if isOpt {
		ps.Score = pi.Bound
		ps.Solution = make([]byte, pi.N)
	} else {
		ps.Score = 0.0
		ps.Solution = make([]byte, 0, pi.N)
	}
	ps.T = 0
	ps.start = time.Now()
	ps.tillCheck = 0
	ps.fixed = false
	return ps, nil

}

func CopyProblemSolution(ps *ProblemSolution) *ProblemSolution {
	clone := new(ProblemSolution)
	clone.Score = ps.Score
	clone.Solution = make([]byte, len(ps.Solution))
	copy(clone.Solution, ps.Solution)
	clone.T = ps.T
	clone.start = ps.start
	clone.tillCheck = ps.tillCheck
	clone.fixed = ps.fixed
	return clone
}

//FixProblemSolution deal with the offset and negates the score based on seekMin
func FixProblemSolution(pi *ProblemInstance, ps *ProblemSolution) {
	if ps.fixed {
		return
	}
	ps.Score = ps.Score + float64(pi.N-1)*pi.Offset
	if !pi.SeekMin {
		ps.Score *= -1.0
	}
	ps.fixed = true
	ps.T = time.Since(ps.start)
}

//OutOfTime decides whether a partial solution is out of time
func OutOfTime(pi *ProblemInstance, ps *ProblemSolution) bool {
	// fmt.Println("Checking time")
	// fmt.Printf("%v", time.Since(ps.start))
	if ps.tillCheck == pi.maxTillCheck {
		// fmt.Println("checking really")
		ps.T = time.Since(ps.start)
		if ps.T > pi.T {
			// fmt.Println(true)
			return true
		}
		ps.tillCheck = 0
	} else {
		ps.tillCheck += 1
		// fmt.Println(ps.tillCheck)
	}
	return false
}

//NormalizeBiases takes a 2d slice of biases, finds the minimum bias,
//subtracts this from each cell and returns that bias. (The new minimum bias should be approximately 0)
func NormalizeProblemInstance(pi *ProblemInstance) {
	minimum := math.Inf(1)
	for _, biasRow := range pi.Bias {
		for _, f := range biasRow {
			minimum = math.Min(minimum, f)
		}
	}
	for i, biasRow := range pi.Bias {
		for j, _ := range biasRow {
			pi.Bias[i][j] -= minimum
		}
	}
	pi.Bound -= minimum * float64(pi.N-1)
	pi.Offset = minimum
}

func ScoreSolution(bias [][]float64, partSol *ProblemSolution) error {
	seq := partSol.Solution
	partSol.Score = 0
	for i, codL := range seq[:len(seq)-1] { //Stop one early to avoid out of bounds
		codR := seq[i+1]
		if 0 <= codL && codL < 64 {
			if 0 <= codR && codR < 64 {
				partSol.Score += bias[codL][codR]
			} else {
				return errors.New(fmt.Sprintf("Invalid codon identifier %d", codR))
			}
		} else {
			return errors.New(fmt.Sprintf("Invalid codon identifier %d", codR))
		}
	}
	return nil
}

func CheckSolution(pi *ProblemInstance, ps *ProblemSolution) error {
	piCount := make([]int, 64)
	for _, codon := range pi.Seq {
		piCount[codon]++
	}
	solCount := make([]int, 64)
	if len(pi.Seq) != pi.N || len(ps.Solution) != pi.N {
		fmt.Printf("pi.seq %d pi.N %d ps.Solution %d", len(pi.Seq), pi.N, len(ps.Solution))
	}
	for _, codon := range ps.Solution {
		solCount[codon]++
	}
	for codon, countInPi := range piCount {
		if countInPi != solCount[codon] {
			codStr, ok := u.CodonString(byte(codon))
			if !ok {
				return errors.New(fmt.Sprintf("Failed to check solution because problem instance contains invalid codon identifier %d", codon))
			}
			return errors.New(fmt.Sprintf("Solution contains %d instances of %s (%d) while original contains 5 %d.",
				solCount[codon], codStr, codon, countInPi))
		}
	}
	return nil
}

type GreedyNode struct {
	Position   int
	CodonLeft  byte
	Codon      byte
	CodonRight byte
	Score      float64
}

type GNByScore []*GreedyNode

func (gns GNByScore) Len() int {
	return len(gns)
}

func (gns GNByScore) Swap(i, j int) {
	gns[i], gns[j] = gns[j], gns[i]
}

func (gns GNByScore) Less(i, j int) bool {
	return gns[i].Score < gns[j].Score
}

type ViterbiCache struct {
	active        map[usableBitVector][][]float64
	inactive      map[usableBitVector][][]float64
	maxCount      int
	activeCount   int
	inactiveCount int
	cacheWipes    int
	cacheHit      int64
	cacheMiss     int64
}

func makeViterbiCache(maxCount int) *ViterbiCache {
	vc := new(ViterbiCache)
	vc.active = make(map[usableBitVector][][]float64)
	vc.inactive = make(map[usableBitVector][][]float64)
	vc.maxCount = maxCount
	vc.activeCount = 0
	vc.inactiveCount = 0
	return vc
}

func (vc *ViterbiCache) GetViterbi(usable usableBitVector, pi *ProblemInstance) ([][]float64, error) {
	vit, ok := vc.active[usable]
	vc.cacheHit++ //TODO: Remove for efficiency
	if ok {
		return vit, nil
	}
	vit, ok = vc.inactive[usable]
	if ok {
		vc.active[usable] = vit
		delete(vc.inactive, usable)
		vc.activeCount++
		return vit, nil
	}
	vit, err := viterbi(usable, pi)
	if err != nil {
		return vit, err
	}
	vc.cacheHit--  //TODO: Remove for efficiency
	vc.cacheMiss++ //TODO: Remove for efficiency
	vc.activeCount++
	if vc.activeCount >= vc.maxCount/2 { //make active the inactive and make new active
		vc.cacheWipes++
		vc.active, vc.inactive = make(map[usableBitVector][][]float64), vc.active
		vc.activeCount, vc.inactiveCount = 0, vc.activeCount
	}
	vc.active[usable] = vit

	return vit, nil
}

type GreedyKey struct {
	Count          [64]uint8
	LastCodonIndex byte
}

type GreedyCache struct {
	active        map[GreedyKey]float64
	inactive      map[GreedyKey]float64
	maxCount      int
	activeCount   int
	inactiveCount int
	atg           map[string][]*GreedyNode
	en            [][][]*GreedyNode
	cacheWipes    int
	cacheHit      int64
	cacheMiss     int64
	index         int
}

func makeGreedyCache(maxCount int, atg map[string][]*GreedyNode, en [][][]*GreedyNode) *GreedyCache {
	gc := new(GreedyCache)
	gc.active = make(map[GreedyKey]float64)
	gc.inactive = make(map[GreedyKey]float64)
	gc.maxCount = maxCount
	gc.activeCount = 0
	gc.inactiveCount = 0
	gc.atg = atg
	gc.en = en
	return gc
}

func (gc *GreedyCache) Wipe() {
	gc.active = make(map[GreedyKey]float64)
	gc.inactive = make(map[GreedyKey]float64)
	gc.activeCount = 0
	gc.inactiveCount = 0
}

func (gc *GreedyCache) GetGreedy(pi *ProblemInstance, dn *DynamicNode) float64 {
	if dn.Index > gc.index {
		gc.index = dn.Index
		gc.Wipe()
	}
	key := GreedyKey{dn.Count, dn.LastCodonIndex}
	score, ok := gc.active[key]
	gc.cacheHit++ //TODO: Remove for efficiency
	if ok {
		return score
	}
	score, ok = gc.inactive[key]
	if ok {
		gc.active[key] = score
		delete(gc.inactive, key)
		gc.activeCount++
		return score
	}
	score = scoreGreedy(pi, dn.Count[:], dn.Index, dn.LastCodonIndex, gc.atg, gc.en)
	gc.cacheHit--  //TODO: Remove for efficiency
	gc.cacheMiss++ //TODO: Remove for efficiency
	gc.activeCount++
	if gc.activeCount >= gc.maxCount/2 { //make active the inactive and make new active
		gc.cacheWipes++
		gc.active, gc.inactive = make(map[GreedyKey]float64), gc.active
		gc.activeCount, gc.inactiveCount = 0, gc.activeCount
	}
	gc.active[key] = score

	return score
}

type ANode struct {
	R              *ReconstructNode
	partScore      float64
	lowerBound     float64
	Count          [64]uint8
	Usable         usableBitVector
	index          int
	lastCodonIndex byte
}

type AHeap []*ANode

func (h AHeap) Len() int {
	return len(h)
}

func (h AHeap) Less(i, j int) bool {
	//return h[i].lowerBound < h[j].lowerBound
	if h[i].lowerBound < h[j].lowerBound {
		return true
	}
	if h[i].lowerBound == h[j].lowerBound && h[i].index > h[j].index {
		return true
	}
	/*if h[i].lowerBound == h[j].lowerBound && h[i].index == h[j].index && h[i].lastCodonIndex < h[j].lastCodonIndex {
		return true
	}*/
	return false
}

func (h AHeap) Swap(i, j int) {
	h[i], h[j] = h[j], h[i]
}

func (h *AHeap) Push(x interface{}) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(*ANode))
}

func (h *AHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}
