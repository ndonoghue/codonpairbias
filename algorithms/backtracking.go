//codonPairBias/algorithms contains several  algorithms for optimizing codon pair bias while holding global codon bias constant.
package algorithms

import (
	u "codonPairBias/utilities"
	"errors"
	"fmt"
	"math"
)

func NaiveBacktracking(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("NaiveBacktracking")
	NormalizeProblemInstance(pi)
	partSol, err := MakeProblemSolution(pi, false)
	if err != nil {
		return partSol, err
	}
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}
	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, partSolu *ProblemSolution) bool {
		return partSolu.Score > optSolu.Score && !u.NearlyEquals(partSolu.Score, optSolu.Score)
	}
	err = helperBacktracking(pi, optSol, partSol, noHope, 0)
	FixProblemSolution(pi, optSol)
	return optSol, err
}

func StaticViterbiBacktracking(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("StaticViterbiBacktracking")
	NormalizeProblemInstance(pi)
	partSol, err := MakeProblemSolution(pi, false)
	if err != nil {
		return partSol, err
	}
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}

	vit, err := viterbi(pi.Usable, pi)
	if err != nil {
		return optSol, err
	}

	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, partSolu *ProblemSolution) bool {
		if partSolu.Score > optSolu.Score && !u.NearlyEquals(partSolu.Score, optSolu.Score) {
			return true
		}
		lastIndex := len(partSolu.Solution) - 1
		if lastIndex == -1 {
			min := u.Inf
			for _, f := range vit[0] {
				min = math.Min(min, f)
			}
			score := partSolu.Score + min
			return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
		}
		/*lastCodon := partSolu.Solution[lastIndex]
		var lastCodonIndex int
		codOpts := probInst.Options[lastIndex]
		for lastCodonIndex = 0; lastCodonIndex < len(codOpts); lastCodonIndex++ {
			if lastCodon == codOpts[lastCodonIndex] {
				break
			}
		}*/
		score := partSolu.Score + vit[lastIndex][partSolu.LastChosenIndex]
		return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
	}
	err = helperBacktracking(pi, optSol, partSol, noHope, 0)
	FixProblemSolution(pi, optSol)
	return optSol, err

}

func DynamicViterbiBacktracking(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("DynamicViterbiBacktracking")
	NormalizeProblemInstance(pi)
	partSol, err := MakeProblemSolution(pi, false)
	if err != nil {
		return partSol, err
	}
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}

	vit, err := viterbi(pi.Usable, pi)
	if err != nil {
		return optSol, err
	}
	vc := makeViterbiCache(pi.MaxCache)

	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, partSolu *ProblemSolution) bool {
		if partSolu.Score > optSolu.Score && !u.NearlyEquals(partSolu.Score, optSolu.Score) {
			return true
		}
		lastIndex := len(partSolu.Solution) - 1
		if lastIndex == -1 {
			min := u.Inf
			for _, f := range vit[0] {
				min = math.Min(min, f)
			}
			score := partSolu.Score + min
			return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
		}
		score := partSolu.Score + vit[lastIndex][partSolu.LastChosenIndex]
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			return true
		}
		dynVit, _ := vc.GetViterbi(probInst.Usable, probInst)
		score = partSolu.Score + dynVit[lastIndex][partSolu.LastChosenIndex]
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			return true
		}
		return false
	}
	err = helperBacktracking(pi, optSol, partSol, noHope, 0)
	FixProblemSolution(pi, optSol)
	fmt.Printf("Cache Misses %d\n", vc.cacheMiss)
	fmt.Printf("Cache Hits %d\n", vc.cacheHit)
	fmt.Printf("Cache Hit Rate %f\n", float64(vc.cacheHit)/float64(vc.cacheMiss+vc.cacheHit))
	fmt.Printf("Cache Wipes %d\n", vc.cacheWipes)

	return optSol, err

}

func GreedyBacktracking(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("GreedyBacktracking")
	NormalizeProblemInstance(pi)
	partSol, err := MakeProblemSolution(pi, false)
	if err != nil {
		return partSol, err
	}
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}

	aminoToGreedy, edgeNodes := greedySetup(pi)

	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, partSolu *ProblemSolution) bool {
		if partSolu.Score > optSolu.Score && !u.NearlyEquals(partSolu.Score, optSolu.Score) {
			return true
		}
		score := partSolu.Score
		score += scoreGreedy(pi, pi.UCount[:], len(partSolu.Solution)-1, byte(partSolu.LastChosenIndex), aminoToGreedy, edgeNodes)
		return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
	}
	err = helperBacktracking(pi, optSol, partSol, noHope, 0)
	FixProblemSolution(pi, optSol)
	return optSol, err

}

func StaticViterbiGreedyBacktracking(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("StaticViterbiGreedyBacktracking")
	NormalizeProblemInstance(pi)
	partSol, err := MakeProblemSolution(pi, false)
	if err != nil {
		return partSol, err
	}
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}

	aminoToGreedy, edgeNodes := greedySetup(pi)
	vit, err := viterbi(pi.Usable, pi)
	if err != nil {
		return optSol, err
	}
	betterCount := 0
	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, partSolu *ProblemSolution) bool {
		if partSolu.Score > optSolu.Score && !u.NearlyEquals(partSolu.Score, optSolu.Score) {
			return true
		}
		score := partSolu.Score
		vitScore := partSolu.Score

		lastIndex := len(partSolu.Solution) - 1
		if lastIndex == -1 {
			min := u.Inf
			for _, f := range vit[0] {
				min = math.Min(min, f)
			}
			vitScore += min
			if vitScore > optSolu.Score && !u.NearlyEquals(vitScore, optSolu.Score) {
				return true
			}

		} else {
			vitScore += vit[lastIndex][partSolu.LastChosenIndex]
			if vitScore > optSolu.Score && !u.NearlyEquals(vitScore, optSolu.Score) {
				return true
			}
		}

		score += scoreGreedy(pi, pi.UCount[:], len(partSolu.Solution)-1, byte(partSolu.LastChosenIndex), aminoToGreedy, edgeNodes)
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			betterCount++
			return true
		}
		return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)

	}
	err = helperBacktracking(pi, optSol, partSol, noHope, 0)
	fmt.Printf("Greedy Helped %d\n", betterCount)
	FixProblemSolution(pi, optSol)
	return optSol, err

}
func DynamicViterbiGreedyBacktracking(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("DynamicViterbiGreedyBacktracking")
	NormalizeProblemInstance(pi)
	partSol, err := MakeProblemSolution(pi, false)
	if err != nil {
		return partSol, err
	}
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}

	vit, err := viterbi(pi.Usable, pi)
	if err != nil {
		return optSol, err
	}
	vc := makeViterbiCache(pi.MaxCache)
	aminoToGreedy, edgeNodes := greedySetup(pi)
	dynBetter, greedyBetter := 0, 0

	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, partSolu *ProblemSolution) bool {
		if partSolu.Score > optSolu.Score && !u.NearlyEquals(partSolu.Score, optSolu.Score) {
			return true
		}
		lastIndex := len(partSolu.Solution) - 1
		if lastIndex == -1 {
			min := u.Inf
			for _, f := range vit[0] {
				min = math.Min(min, f)
			}
			score := partSolu.Score + min
			return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
		}
		score := partSolu.Score + vit[lastIndex][partSolu.LastChosenIndex]
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			return true
		}
		dynVit, _ := vc.GetViterbi(probInst.Usable, probInst)
		score = partSolu.Score + dynVit[lastIndex][partSolu.LastChosenIndex]
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			dynBetter++
			return true
		}
		if len(partSolu.Solution) > pi.N/4 {
			return false
		}
		score = partSolu.Score + scoreGreedy(pi, pi.UCount[:], len(partSolu.Solution)-1,
			byte(partSolu.LastChosenIndex), aminoToGreedy, edgeNodes)
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			greedyBetter++
			return true
		}
		return false
	}
	err = helperBacktracking(pi, optSol, partSol, noHope, 0)
	FixProblemSolution(pi, optSol)
	fmt.Printf("Cache Misses %d\n", vc.cacheMiss)
	fmt.Printf("Cache Hits %d\n", vc.cacheHit)
	fmt.Printf("Cache Hit Rate %f\n", float64(vc.cacheHit)/float64(vc.cacheMiss+vc.cacheHit))
	fmt.Printf("Cache Wipes %d\n", vc.cacheWipes)
	fmt.Printf("Dynamic Viterbi Helped %d\n", dynBetter)
	fmt.Printf("Greedy Helped %d\n", greedyBetter)
	return optSol, err

}

func helperBacktracking(pi *ProblemInstance, optSol *ProblemSolution,
	partSol *ProblemSolution, noHope solutionEvaluator, depth int) error {
	if OutOfTime(pi, optSol) {
		return errors.New(fmt.Sprintf("Backtracking failed to complete in %v at depth %d.", optSol.T, depth))
	}
	if isSolution(pi, partSol) {
		err := processSolution(pi, optSol, partSol)
		return err
	}
	if deadPartial(pi, optSol, partSol, noHope) {
		return nil
	}
	nextCods := pi.Options[depth]
	partSol.Solution = append(partSol.Solution, 0) // Grow solution vector
	for index, nextCod := range nextCods {         // Explore children
		if pi.Count[nextCod] <= 0 {
			continue //invalid child
		}
		//Prepare child
		pi.Count[nextCod] -= 1
		pi.UCount[nextCod] -= 1
		if pi.Count[nextCod] == 0 { //nextCod exhausted
			pi.Usable = pi.Usable.exhaust(nextCod)
		}
		partSol.Solution[depth] = nextCod
		partSol.LastChosenIndex = index
		if depth > 0 {
			partSol.Score += pi.Bias[partSol.Solution[depth-1]][partSol.Solution[depth]]
		}
		//End Prepare child
		err := helperBacktracking(pi, optSol, partSol, noHope, depth+1)
		if err != nil {
			return err
		}
		//Forget child
		pi.Count[nextCod] += 1
		if pi.Count[nextCod] == 1 { //nextCod replenished
			pi.Usable = pi.Usable.replenish(nextCod)
		}
		pi.UCount[nextCod] += 1
		if depth > 0 {
			partSol.Score -= pi.Bias[partSol.Solution[depth-1]][partSol.Solution[depth]]
		}
		partSol.Solution = partSol.Solution[:depth+1]
		//End forget child
	}

	return nil
}

func isSolution(pi *ProblemInstance, partSol *ProblemSolution) bool {
	if len(partSol.Solution) == pi.N {
		return true
	}
	return false
}

func processSolution(pi *ProblemInstance, optSol *ProblemSolution, partSol *ProblemSolution) error {
	/*sol := make([]string, len(partSol.Solution))
	for i, codNum := range partSol.Solution {
		codStr, ok := u.CodonString(codNum)
		if !ok {
			return errors.New(fmt.Sprintf("Invalid codon identifier %d", codNum))
		}
		sol[i] = codStr
	}*/

	err := ScoreSolution(pi.Bias, partSol)
	if err != nil {
		return err
	}
	if partSol.Score <= optSol.Score || u.NearlyEquals(partSol.Score, optSol.Score) {
		copy(optSol.Solution, partSol.Solution)
		optSol.Score = partSol.Score
		fmt.Printf("Found %f\n", optSol.Score+pi.Offset*float64(pi.N-1))
		//fmt.Println("Processing", sol)
	}
	return nil
}

func deadPartial(pi *ProblemInstance, optSol *ProblemSolution, partSol *ProblemSolution,
	noHope solutionEvaluator) bool {
	if noHope(pi, optSol, partSol) {
		return true
	} else {
		return false
	}
}
