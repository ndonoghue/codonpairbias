package algorithms

import (
	u "codonPairBias/utilities"
	"fmt"
)

func Random(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("Random")
	ps, err := MakeProblemSolution(pi, false)
	for _, opts := range pi.Options {
		for _, cod := range opts {
			fmt.Print(cod, " ")
		}
		fmt.Println()
	}
	fmt.Println()
	for _, cod := range pi.Seq {
		fmt.Println(cod)
	}
	//ps.Solution = pi.Seq
	if err != nil {
		return ps, err
	}
	classToPos := getClassToPos(pi)
	used := make([]bool, pi.N) //records whether the codon in the original sequence has been used in the new one
	randSeq := make([]byte, pi.N)
	for i, origCodon := range pi.Seq { //iterate through positions in new sequence, filling each one, respecting class
		class := pi.CodonToAmino[origCodon]
		posSlice := classToPos[class]
		j := u.RIntn(len(posSlice))
		for used[posSlice[j]] { //walk along till we find an unused codon
			j++
			j %= len(posSlice)
		}
		randSeq[i] = pi.Seq[posSlice[j]]
		used[posSlice[j]] = true
	}
	ps.Solution = randSeq
	ScoreSolution(pi.Bias, ps)
	FixProblemSolution(pi, ps)
	return ps, nil
}
