package algorithms

import (
	"errors"
	"fmt"
)

func GreedyHeuristic(pi *ProblemInstance) (*ProblemSolution, error) {
	didSwap := true
	swapCount := 0
	ps, err := MakeProblemSolution(pi, false)
	ps.Solution = make([]byte, pi.N)
	copy(ps.Solution, pi.Seq)
	ScoreSolution(pi.Bias, ps)
	if err != nil {
		return ps, err
	}
	classToPos := getClassToPos(pi)
	for didSwap && !OutOfTime(pi, ps) {
		didSwap = makeBestSwap(pi, ps, classToPos)
		if didSwap {
			swapCount++
		}
	}
	if OutOfTime(pi, ps) {
		err = errors.New(fmt.Sprintf("Greedy heuristic failed to find local optimum."))
	}
	ScoreSolution(pi.Bias, ps)
	FixProblemSolution(pi, ps)
	return ps, err
}

func makeBestSwap(pi *ProblemInstance, ps *ProblemSolution, classToPos map[string][]int) bool {
	swap1, swap2, delta := findBestSwap(pi, ps, classToPos)
	if swap1 == swap2 {
		return false
	}
	makeSwap(swap1, swap2, delta, ps)
	return true
}
