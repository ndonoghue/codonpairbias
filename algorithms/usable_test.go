//codonPairBias/algorithms contains several  algorithms for optimizing codon pair bias while holding global codon bias constant.
package algorithms

import (
	"errors"
	"fmt"
	"testing"
)

func TestReplenish(t *testing.T) {
	var usable usableBitVector //starts off with nothing usable
	var i uint8
	for i = 0; i < 64; i++ {
		usable = usable.replenish(i)
		if !usable.isUsable(i) {
			t.Error(errors.New(fmt.Sprintf("%d failed to replenish", i)))
		}
		usable.replenish(i)
		if !usable.isUsable(i) {
			t.Error(errors.New(fmt.Sprintf("%d failed to replenish idempotently", i)))
		}
	}
	var j uint8
	for i = 0; i < 64; i++ {
		usable = 0x0000000000000000
		usable = usable.replenish(i)
		for j = 0; j < 64; j++ {
			if i != j && usable.isUsable(j) {
				t.Error(errors.New(fmt.Sprintf("%d made usable by replenishing %d", j, i)))
			}
		}
	}
}

func TestExhaust(t *testing.T) {
	var usable usableBitVector //starts off with nothing usable
	usable = ^usable           //should be all usable now
	var i uint8
	for i = 0; i < 64; i++ {
		usable = usable.exhaust(i)
		if usable.isUsable(i) {
			t.Error(errors.New(fmt.Sprintf("%d failed to exhaust", i)))
		}
		usable = usable.exhaust(i)
		if usable.isUsable(i) {
			t.Error(errors.New(fmt.Sprintf("%d failed to exhaust idempotently", i)))
		}
	}
}

func TestIsUsable(t *testing.T) {
	var usable usableBitVector //starts  offf  witht nothing usable
	var i uint8
	for i = 0; i < 64; i++ {
		if usable.isUsable(i) {
			t.Error(errors.New(fmt.Sprintf("%d was inappropriately usable", i)))
		}
	}
	usable = ^usable
	for i = 0; i < 64; i++ {
		if !usable.isUsable(i) {
			t.Error(errors.New(fmt.Sprintf("%d was inappropriately unusable", i)))
		}
	}
	var j uint8
	for i = 0; i < 64; i++ {
		usable = 0x0000000000000000
		usable = ^usable
		usable = usable.exhaust(i)
		for j = 0; j < 64; j++ {
			if i != j && !usable.isUsable(j) {
				t.Error(errors.New(fmt.Sprintf("%d made unusable by exhausting %d", j, i)))
			}
		}
	}
}

func TestMakeUsableBitVector(t *testing.T) {
	count := make([]int, 64)
	var i uint8
	usable := makeUsableBitVector(count)
	for i = 0; i < 64; i++ {
		if usable.isUsable(i) {
			t.Error(errors.New(fmt.Sprintf("%d was inappropriately usable", i)))
		}
	}
	codonCount := 0
	for i := 0; i < 64; i++ {
		count[i] = codonCount - 2
		codonCount++
		codonCount %= 5
	}
	usable = makeUsableBitVector(count)
	codonCount = 0
	for i = 0; i < 64; i++ {
		if usable.isUsable(i) {
			if i%5 <= 2 {
				t.Error(errors.New(fmt.Sprintf("%d was inappropriately usable", i)))
			}
		} else {
			if i%5 > 2 {
				t.Error(errors.New(fmt.Sprintf("%d was inappropriately unusable", i)))
			}
		}
	}
	codonCount = 0
	for i = 0; i < 64; i++ {
		count[i] = codonCount
		codonCount++
		codonCount %= 7
	}
	usable = makeUsableBitVector(count)
	for i = 0; i < 64; i++ {
		if usable.isUsable(i) {
			if i%7 == 0 {
				t.Error(errors.New(fmt.Sprintf("%d was inappropriately usable", i)))
			}
		} else {
			if i%7 != 0 {
				t.Error(errors.New(fmt.Sprintf("%d was inappropriately unusable", i)))
			}
		}
	}

}
