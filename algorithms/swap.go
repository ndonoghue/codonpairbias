package algorithms

import u "codonPairBias/utilities"

func getClassToPos(pi *ProblemInstance) map[string][]int {
	classToPos := make(map[string][]int)
	for _, class := range pi.Aminos {
		posSlice := make([]int, 0)
		for pos, cod := range pi.Seq {
			if pi.CodonToAmino[cod] == class { //cod's amino is the same as the class's amino
				posSlice = append(posSlice, pos)
			}
		}
		classToPos[class] = posSlice
	}
	return classToPos
}

func getSwapDelta(pi *ProblemInstance, ps *ProblemSolution, i int, j int) float64 {
	delta := 0.0
	if i == j {
		return delta
	}
	if i > j {
		return getSwapDelta(pi, ps, j, i) //i < j for convenience
	}
	codI, codJ := ps.Solution[i], ps.Solution[j]
	if i > 0 { //avoid out of bounds on left
		delta -= pi.Bias[ps.Solution[i-1]][codI] //take away cps for i from left
		delta += pi.Bias[ps.Solution[i-1]][codJ] //add cps for i-1 to j
	}
	if i == j-1 {
		delta -= pi.Bias[codI][codJ] //take away cps for i to j
		delta += pi.Bias[codJ][codI] //add cps for j to i
	} else {
		delta -= pi.Bias[codI][ps.Solution[i+1]] //take away cps for i to right
		delta += pi.Bias[codJ][ps.Solution[i+1]] //add cps for j to i+1
		delta -= pi.Bias[ps.Solution[j-1]][codJ] //take away cps for j from left
		delta += pi.Bias[ps.Solution[j-1]][codI] //add cps for j-1 to i
	}
	if j+1 < pi.N { //avoid out of bounds on right
		delta -= pi.Bias[codJ][ps.Solution[j+1]] //take away cps for j to right
		delta += pi.Bias[codI][ps.Solution[j+1]] //add cps for i to j+1
	}

	return delta
}

func findBestSwap(pi *ProblemInstance, ps *ProblemSolution, classToPos map[string][]int) (int, int, float64) {
	bestSwap1, bestSwap2 := 0, 0
	bestSwapDelta := 0.0 //doing nothing is a delta of 0 and this is always possible
	for _, posSlice := range classToPos {
		for i, pos1 := range posSlice {
			for j := i + 1; j < len(posSlice); j++ {
				pos2 := posSlice[j]
				delta := getSwapDelta(pi, ps, pos1, pos2)
				if delta < bestSwapDelta {
					bestSwap1, bestSwap2 = pos1, pos2
					bestSwapDelta = delta
				}
			}
		}
	}
	return bestSwap1, bestSwap2, bestSwapDelta
}

func findRandomSwap(pi *ProblemInstance, ps *ProblemSolution, classToPos map[string][]int) (int, int, float64) {
	foundSwap := false
	i, j := 0, 0
	for !foundSwap {
		i = u.RIntn(pi.N)
		class := pi.CodonToAmino[ps.Solution[i]]
		posSlice := classToPos[class]
		if len(posSlice) <= 1 {
			continue
		}
		k := u.RIntn(len(posSlice))
		j = posSlice[k]
		if ps.Solution[i] == ps.Solution[j] {
			continue
		} else {
			foundSwap = true
		}
	}
	delta := getSwapDelta(pi, ps, i, j)
	return i, j, delta
}

func makeSwap(pos1 int, pos2 int, delta float64, ps *ProblemSolution) {
	ps.Solution[pos1], ps.Solution[pos2] = ps.Solution[pos2], ps.Solution[pos1]
	ps.Score += delta
}
