//codonPairBias/algorithms contains several  algorithms for optimizing codon pair bias while holding global codon bias constant.
package algorithms

import (
	u "codonPairBias/utilities"
	"errors"
	"fmt"
	"math"
)

func NaiveDynamic(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("NaiveDynamic")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}

	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, dn *DynamicNode) bool {
		return dn.Score > optSolu.Score && !u.NearlyEquals(dn.Score, optSolu.Score)
	}
	err = helperDynamic(pi, optSol, noHope)
	return optSol, err
}

func helperDynamic(pi *ProblemInstance, optSol *ProblemSolution,
	noHope dynamicNodeEvaluator) error {
	oldMap := MakeDynamicNodeMap(0, pi)
	newMap := MakeDynamicNodeMap(0, pi)
	for lastCodonIndex, codon := range pi.Options[0] { //Make the initial set of nodes for depth 0
		dn := new(DynamicNode)
		for i := 0; i < 64; i++ {
			dn.Count[i] = uint8(pi.Count[i])
		}
		if dn.Count[codon] <= 0 { //exhausted before placing
			continue
		}
		dn.Count[codon] -= 1 //update count
		dn.Usable = pi.Usable
		if dn.Count[codon] == 0 {
			dn.Usable = dn.Usable.exhaust(codon)
		}
		dn.LastCodonIndex = byte(lastCodonIndex)
		dn.Reconstruct = &ReconstructNode{nil, codon}
		newMap.ConditionalUpdate(dn)
	}

	for index, optionsAt := range pi.Options[1:] { //skip 0th index, covered above
		index++ //TODO: clean up this? Standardizes index to match optionsAt
		oldMap = newMap
		fmt.Printf("%d\t%d\n", index, len(oldMap.Slice))
		//fmt.Println(len(oldMap.Slice))
		newMap = MakeDynamicNodeMap(index, pi) //TODO: leave here or put at bottom?
		for _, dn := range oldMap.Slice {      //Iterate over nodes in previous level, generating children
			if OutOfTime(pi, optSol) {
				return errors.New(fmt.Sprintf("Dynamic programming failed to complete in %v at depth %d.", optSol.T, index))
			}

			codL := byte(pi.Options[index-1][dn.LastCodonIndex])
			for lastCodonIndex, codR := range optionsAt {
				child := new(DynamicNode)
				if dn.Count[codR] <= 0 { //Too many codR's used
					continue
				}
				child.Count = dn.Count //TODO: Make sure this copies array like it's supposed to
				child.Count[codR] -= 1
				child.Usable = dn.Usable
				if child.Count[codR] <= 0 {
					child.Usable = dn.Usable.exhaust(codR)
				}
				child.Index = index
				child.LastCodonIndex = byte(lastCodonIndex)
				child.Score = dn.Score + pi.Bias[codL][codR]
				//child.Parent = dn
				if noHope(pi, optSol, child) {
					continue
				}
				child.Reconstruct = &ReconstructNode{dn.Reconstruct, codR}
				newMap.ConditionalUpdate(child) //TODO: need the returns for anything?
			}
		}
	}
	fmt.Printf("%d\t%d\n", pi.N, len(newMap.Slice))
	optCost := u.Inf //pi.Bound
	var optDn *DynamicNode
	optDn = nil
	for _, dn := range newMap.Slice {
		if optCost >= dn.Score || u.NearlyEquals(optCost, dn.Score) {
			optDn = dn
			optCost = dn.Score
		}
	}
	if optDn == nil {
		return errors.New(fmt.Sprintf("Error. No solution found better than or equal to the bound"))
	}

	optSol.Score = optDn.Score
	optSol.Solution = make([]uint8, pi.N)
	err := optDn.Reconstruct.ReconstructSequence(optSol.Solution)
	if err != nil {
		return err
	}
	/*for i := pi.N - 1; i >= 0; i-- {
		optSol.Solution[i] = pi.Options[i][optDn.LastCodonIndex]
		optDn = optDn.Parent
	}*/

	return nil
}

func StaticViterbiDynamic(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("StaticViterbiDynamic")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}
	vit, err := viterbi(pi.Usable, pi)
	if err != nil {
		return optSol, err
	}
	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, dn *DynamicNode) bool {
		if dn.Score > optSolu.Score && !u.NearlyEquals(dn.Score, optSolu.Score) {
			return true
		}
		lastIndex := dn.Index
		if lastIndex == -1 {
			min := u.Inf
			for _, f := range vit[0] {
				min = math.Min(min, f)
			}
			score := dn.Score + min
			return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
		}
		score := dn.Score + vit[lastIndex][dn.LastCodonIndex]
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			//fmt.Printf("%f score %f optScore\n", score, optSolu.Score)
		}
		return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
	}

	err = helperDynamic(pi, optSol, noHope)
	FixProblemSolution(pi, optSol)
	return optSol, err
}

func DynamicViterbiDynamic(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("DynamicViterbiDynamic")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}
	vit, err := viterbi(pi.Usable, pi)
	if err != nil {
		return optSol, err
	}
	vc := makeViterbiCache(pi.MaxCache)
	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, dn *DynamicNode) bool {
		if dn.Index == probInst.N-1 {
			return false
		}
		if dn.Score > optSolu.Score && !u.NearlyEquals(dn.Score, optSolu.Score) {
			return true
		}
		lastIndex := dn.Index
		/*if lastIndex == -1 {
			min := u.Inf
			for _, f := range vit[0] {
				min = math.Min(min, f)
			}
			score := dn.Score + min
			return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
		}*/
		score := dn.Score + vit[lastIndex][dn.LastCodonIndex]
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			return true //fmt.Printf("%f score %f optScore\n", score, optSolu.Score)
		}
		dynVit, _ := vc.GetViterbi(dn.Usable, probInst)
		score = dn.Score + dynVit[lastIndex][dn.LastCodonIndex]
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			return true
		}

		return false
	}

	err = helperDynamic(pi, optSol, noHope)
	fmt.Printf("Cache Misses %d\n", vc.cacheMiss)
	fmt.Printf("Cache Hits %d\n", vc.cacheHit)
	fmt.Printf("Cache Hit Rate %f\n", float64(vc.cacheHit)/float64(vc.cacheMiss+vc.cacheHit))
	fmt.Printf("Cache Wipes %d\n", vc.cacheWipes)
	FixProblemSolution(pi, optSol)
	return optSol, err
}

func DynamicViterbiGreedyDynamic(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("DynamicViterbiGreedyDynamic")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}
	vit, err := viterbi(pi.Usable, pi)
	if err != nil {
		return optSol, err
	}
	vc := makeViterbiCache(pi.MaxCache)
	aminoToGreedy, edgeNodes := greedySetup(pi)
	gc := makeGreedyCache(pi.MaxCache, aminoToGreedy, edgeNodes)
	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, dn *DynamicNode) bool {
		if dn.Index == probInst.N-1 {
			return false
		}
		if dn.Score > optSolu.Score && !u.NearlyEquals(dn.Score, optSolu.Score) {
			return true
		}
		lastIndex := dn.Index
		if lastIndex == -1 {
			min := u.Inf
			for _, f := range vit[0] {
				min = math.Min(min, f)
			}
			score := dn.Score + min
			return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
		}
		score := dn.Score + vit[lastIndex][dn.LastCodonIndex]
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			return true //fmt.Printf("%f score %f optScore\n", score, optSolu.Score)
		}
		dynVit, _ := vc.GetViterbi(dn.Usable, probInst)
		score = dn.Score + dynVit[lastIndex][dn.LastCodonIndex]
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			return true
		}
		gScore := gc.GetGreedy(probInst, dn)
		score = dn.Score + gScore
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			return true
		}
		return false
	}

	err = helperDynamic(pi, optSol, noHope)
	fmt.Printf("Cache Misses %d\n", vc.cacheMiss)
	fmt.Printf("Cache Hits %d\n", vc.cacheHit)
	fmt.Printf("Cache Hit Rate %f\n", float64(vc.cacheHit)/float64(vc.cacheMiss+vc.cacheHit))
	fmt.Printf("Cache Wipes %d\n", vc.cacheWipes)

	fmt.Printf("Greedy Cache Misses %d\n", gc.cacheMiss)
	fmt.Printf("Greedy Cache Hits %d\n", gc.cacheHit)
	fmt.Printf("Greedy Cache Hit Rate %f\n", float64(gc.cacheHit)/float64(gc.cacheMiss+gc.cacheHit))
	fmt.Printf("Greedy Cache Wipes %d\n", gc.cacheWipes)
	FixProblemSolution(pi, optSol)
	return optSol, err
}

func GreedyDynamic(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("GreedyDynamic")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}
	aminoToGreedy, edgeNodes := greedySetup(pi)

	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, dn *DynamicNode) bool {

		score := dn.Score
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			return true
		}
		//oldScore := score
		score += scoreGreedy(pi, dn.Count[:], dn.Index, dn.LastCodonIndex, aminoToGreedy, edgeNodes)
		/*if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			fmt.Printf("Bad Score %f\nOpt Score %f\nOld Score %f\nIndex %d\n", score, optSolu.Score, oldScore, dn.Index)
			fmt.Printf("%v", dn)
		}*/
		/*fmt.Printf("ScoreG %f\n", score)
		fmt.Printf("ScoreO %f\n", optSolu.Score)*/
		return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
	}

	err = helperDynamic(pi, optSol, noHope)
	FixProblemSolution(pi, optSol)
	return optSol, err
}

func StaticViterbiGreedyDynamic(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("StaticViterbiGreedyDynamic")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}
	aminoToGreedy, edgeNodes := greedySetup(pi)
	vit, err := viterbi(pi.Usable, pi)
	if err != nil {
		return optSol, err
	}
	betterCount := 0
	avgVit := 0.0
	avgI := 0
	noHope := func(probInst *ProblemInstance, optSolu *ProblemSolution, dn *DynamicNode) bool {

		score := dn.Score
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			return true
		}

		vitScore := score
		// vit
		lastIndex := dn.Index
		if lastIndex == -1 {
			min := u.Inf
			for _, f := range vit[0] {
				min = math.Min(min, f)
			}
			vitScore = dn.Score + min
			if vitScore > optSolu.Score && !u.NearlyEquals(vitScore, optSolu.Score) {
				return true
			}

		} else {
			vitScore += vit[lastIndex][dn.LastCodonIndex]

			if vitScore > optSolu.Score && !u.NearlyEquals(vitScore, optSolu.Score) {
				return true
			}
		}
		//end vit
		/*if dn.Index%1 != 0 && dn.Index < pi.N/2.0+pi.N {
			return false
		}*/
		/*if true {
			return false
		}*/
		score += scoreGreedy(pi, dn.Count[:], dn.Index, dn.LastCodonIndex, aminoToGreedy, edgeNodes)
		if score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score) {
			avgVit += (optSolu.Score - vitScore)
			avgI += dn.Index
			betterCount++
		}
		return score > optSolu.Score && !u.NearlyEquals(score, optSolu.Score)
	}
	err = helperDynamic(pi, optSol, noHope)
	fmt.Printf("Helped %d\n", betterCount)
	fmt.Printf("Avg%f\n", avgVit/float64(betterCount))
	fmt.Printf("AvgI%f\n", float64(avgI)/float64(betterCount))
	FixProblemSolution(pi, optSol)
	return optSol, err
}
