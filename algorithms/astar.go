//codonPairBias/algorithms contains several  algorithms for optimizing codon pair bias while holding global codon bias constant.
package algorithms

import (
	u "codonPairBias/utilities"
	"container/heap"
	"errors"
	"fmt"
)

func NaiveAstar(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("NaiveAstar")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}
	lb := func(probInst *ProblemInstance, a *ANode) float64 {
		return 0.0
	}

	err = helperAstar(pi, optSol, lb)
	FixProblemSolution(pi, optSol)
	return optSol, err
}

func StaticViterbiAstar(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("StaticViterbiAstar")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}
	vit, err := viterbi(pi.Usable, pi)
	if err != nil {
		return optSol, err
	}
	lb := func(probInst *ProblemInstance, a *ANode) float64 {
		score := vit[a.index][a.lastCodonIndex]
		return score
	}

	err = helperAstar(pi, optSol, lb)
	FixProblemSolution(pi, optSol)
	return optSol, err
}

func DynamicViterbiAstar(pi *ProblemInstance) (*ProblemSolution, error) {
	fmt.Println("DynamicViterbiAstar")
	NormalizeProblemInstance(pi)
	optSol, err := MakeProblemSolution(pi, true)
	if err != nil {
		return optSol, err
	}
	vc := makeViterbiCache(pi.MaxCache)
	if err != nil {
		return optSol, err
	}
	lb := func(probInst *ProblemInstance, a *ANode) float64 {
		if a.index == pi.N-1 {
			return 0.0
		}
		vit, err := vc.GetViterbi(a.Usable, probInst)
		if err != nil {
			fmt.Println(err) //TODO: Handle
		}
		score := vit[a.index][a.lastCodonIndex]
		return score
	}
	err = helperAstar(pi, optSol, lb)
	FixProblemSolution(pi, optSol)
	return optSol, err
}

func helperAstar(pi *ProblemInstance, optSol *ProblemSolution, lb aNodelowerBound) error {
	q := new(AHeap)
	sets := make([]map[GreedyKey]struct{}, pi.N)
	for i, _ := range sets {
		sets[i] = make(map[GreedyKey]struct{})
	}
	for codIndex, codon := range pi.Options[0] {
		if pi.Count[codon] <= 0 {
			continue
		}
		a := new(ANode)
		a.Count = pi.UCount //TODO: Make sure this copies array like it's supposed to
		a.Count[codon]--
		a.Usable = pi.Usable
		if a.Count[codon] == 0 {
			a.Usable = a.Usable.exhaust(codon)
		}
		a.index = 0
		a.lastCodonIndex = byte(codIndex)
		a.R = &ReconstructNode{nil, codon}
		a.partScore = 0.0
		a.lowerBound = a.partScore + lb(pi, a)
		if a.lowerBound <= optSol.Score || u.NearlyEquals(a.lowerBound, optSol.Score) {
			heap.Push(q, a)
		}
	}
	var winner *ANode
	winner = nil
	for winner == nil && q.Len() > 0 {
		if OutOfTime(pi, optSol) {
			return errors.New(fmt.Sprintf("A* failed to complete in %v with %d currently plausible nodes.", optSol.T, q.Len()))
		}
		parent := heap.Pop(q).(*ANode)
		if parent.index == pi.N-1 {
			score := parent.lowerBound
			if score <= optSol.Score || u.NearlyEquals(score, optSol.Score) {
				winner = parent
				break
			}
			continue //TODO: Can we break out here?
		}
		key := GreedyKey{parent.Count, parent.lastCodonIndex}
		_, ok := sets[parent.index][key]
		if ok {
			continue // already seen this node with a better score
		}
		var empty struct{}
		sets[parent.index][key] = empty
		nextIndex := parent.index + 1
		for codIndex, codon := range pi.Options[nextIndex] {
			if parent.Count[codon] <= 0 { //already exhausted
				continue
			}
			a := new(ANode)
			a.Count = parent.Count //TODO: Make sure this copies array like it's supposed to
			a.Count[codon]--
			a.Usable = parent.Usable

			a.index = nextIndex
			a.lastCodonIndex = byte(codIndex)
			a.R = &ReconstructNode{parent.R, codon}
			a.partScore = parent.partScore + pi.Bias[parent.R.Choice][codon]
			a.lowerBound = a.partScore + lb(pi, a)
			if a.Count[codon] == 0 {
				a.Usable = a.Usable.exhaust(codon)
			}
			if a.lowerBound <= optSol.Score || u.NearlyEquals(a.lowerBound, optSol.Score) {
				heap.Push(q, a)
			}
		}

	}
	if winner == nil {
		return errors.New(fmt.Sprintf("No optimum solution found"))
	}
	fmt.Println(q.Len())
	optSol.Solution = make([]uint8, pi.N)
	err := winner.R.ReconstructSequence(optSol.Solution)
	ScoreSolution(pi.Bias, optSol)
	return err
}
