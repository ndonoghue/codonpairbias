package algorithms

import (
	u "codonPairBias/utilities"
	"fmt"
)

// var zzMakeRandom bool = true
// var zzInitialTemp float64 = 1.0
// var zzMaxCoolingSteps int = 60000
// var zzMinCoolingSteps int = 30000
// var zzCoolingMult float64 = 0.99
// var zzNormalize float64 = 1000.0
// var zzMinStepsPerTemperature int = 1000
// var zzMaxRuns = 0
// var zzFinishWithGreedy = true
// var yyAfterExtend = false

var zzMakeRandom bool = true
var zzInitialTemp float64 = 1.0
var zzMaxCoolingSteps int = 10000
var zzMinCoolingSteps int = 10000
var zzCoolingMult float64 = 0.99
var zzNormalize float64 = 1000.0
var zzMinStepsPerTemperature int = 1000
var zzMaxRuns = 1
var zzFinishWithGreedy = true
var yyAfterExtend = false

func prepare() {
	if zzMaxRuns > 1 {
		zzMaxCoolingSteps /= zzMaxRuns
		zzMaxCoolingSteps++
		if zzMaxCoolingSteps < zzMinCoolingSteps {
			zzMaxCoolingSteps = zzMinCoolingSteps
		}
	} else if zzMaxRuns == 0 {
		//zzMaxCoolingSteps = zzMinCoolingSteps
	}
	if yyAfterExtend {
		zzMaxRuns = 1
		zzMakeRandom = false
		zzMaxCoolingSteps = 5000
	}
}

func SimulatedAnnealing(pi *ProblemInstance) (*ProblemSolution, error) {
	yyAfterExtend = pi.SimSettings.afterExtend
	prepare()
	outOfTime := false
	pi.maxTillCheck = 1
	ps, err := MakeProblemSolution(pi, false)
	ps.Solution = pi.Seq
	optSol := CopyProblemSolution(ps)
	ScoreSolution(pi.Bias, optSol)
	classToPos := getClassToPos(pi)
	for runs := 0; !outOfTime && (runs < zzMaxRuns || zzMaxRuns == 0); runs++ {

		if zzMakeRandom {
			ps, err = Random(pi)
			if err != nil {
				return ps, err
			}
			ScoreSolution(pi.Bias, ps)
			if ps.Score < optSol.Score {
				optSol = CopyProblemSolution(ps)
			}

		}
		if err != nil {
			return ps, err
		}

		temp := zzInitialTemp
		oldScore := 0.0
		for coolingStep := 0; coolingStep < zzMaxCoolingSteps && !outOfTime; coolingStep++ { //TODO:Add out of time check and other stuff
			outOfTime = OutOfTime(pi, ps)
			ScoreSolution(pi.Bias, ps)
			scoreStartOfRound := ps.Score
			for constantStep := 0; constantStep < zzMinStepsPerTemperature; constantStep++ {
				i, j, delta := findRandomSwap(pi, ps, classToPos)
				if delta < 0.0 || u.RFloat64() < u.Exp(delta/(zzNormalize*temp)) {
					makeSwap(i, j, delta, ps)
					if ps.Score < optSol.Score {
						optSol = CopyProblemSolution(ps)
						ScoreSolution(pi.Bias, optSol)
						for i := 0; i < 5; i++ {
							fmt.Println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$4")
						}
					}
				}
			}
			if scoreStartOfRound <= ps.Score {
				temp *= zzCoolingMult
			} else {
				//fmt.Println("score start ", scoreStartOfRound, " score ", ps.Score)
				coolingStep--
			}
			fmt.Println("Outer loop cycle ", coolingStep)
			if ps.Score < oldScore {
				for i := 0; i < 2; i++ {
					fmt.Println("************************************************")
				}

			}
			oldScore = ps.Score
		}
		copy(ps.Solution, pi.Seq)
		ScoreSolution(pi.Bias, ps)
	}
	optSol.T = ps.T
	FixProblemSolution(pi, optSol)
	if zzFinishWithGreedy {
		copy(pi.Seq, optSol.Solution)
		greedySol, err := GreedyHeuristic(pi)
		if err == nil && greedySol.Score < optSol.Score {
			fmt.Println("Simulated Annealing produced ", optSol.Score)
			fmt.Println("Greedy produced ", greedySol.Score)
			optSol.Score = greedySol.Score
			optSol.Solution = greedySol.Solution
		}
	}
	return optSol, nil
}
