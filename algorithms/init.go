//codonPairBias/algorithms contains several  algorithms for optimizing codon pair bias while holding global codon bias constant.
package algorithms

var StringToAlgorithm map[string]Algorithm

func init() {
	StringToAlgorithm = make(map[string]Algorithm)
	StringToAlgorithm["naivebacktracking"] = NaiveBacktracking
	StringToAlgorithm["nb"] = NaiveBacktracking
	StringToAlgorithm["staticviterbibacktracking"] = StaticViterbiBacktracking
	StringToAlgorithm["svb"] = StaticViterbiBacktracking
	StringToAlgorithm["greedybacktracking"] = GreedyBacktracking
	StringToAlgorithm["gb"] = GreedyBacktracking
	StringToAlgorithm["staticviterbigreedybacktracking"] = StaticViterbiGreedyBacktracking
	StringToAlgorithm["svgb"] = StaticViterbiGreedyBacktracking
	StringToAlgorithm["dynamicviterbibacktracking"] = DynamicViterbiBacktracking
	StringToAlgorithm["dvb"] = DynamicViterbiBacktracking
	StringToAlgorithm["dynamicviterbigreedybacktracking"] = DynamicViterbiGreedyBacktracking
	StringToAlgorithm["dvgb"] = DynamicViterbiGreedyBacktracking

	StringToAlgorithm["naivedynamic"] = NaiveDynamic
	StringToAlgorithm["nd"] = NaiveDynamic
	StringToAlgorithm["staticviterbidynamic"] = StaticViterbiDynamic
	StringToAlgorithm["svd"] = StaticViterbiDynamic
	StringToAlgorithm["greedydynamic"] = GreedyDynamic
	StringToAlgorithm["gd"] = GreedyDynamic
	StringToAlgorithm["staticviterbigreedydynamic"] = StaticViterbiGreedyDynamic
	StringToAlgorithm["svgd"] = StaticViterbiGreedyDynamic
	StringToAlgorithm["dynamicviterbidynamic"] = DynamicViterbiDynamic
	StringToAlgorithm["dvd"] = DynamicViterbiDynamic
	StringToAlgorithm["dynamicviterbigreedydynamic"] = DynamicViterbiGreedyDynamic
	StringToAlgorithm["dvgd"] = DynamicViterbiGreedyDynamic

	StringToAlgorithm["naiveastar"] = NaiveAstar
	StringToAlgorithm["na"] = NaiveAstar
	StringToAlgorithm["staticviterbiastar"] = StaticViterbiAstar
	StringToAlgorithm["sva"] = StaticViterbiAstar

	StringToAlgorithm["dynamicviterbiastar"] = DynamicViterbiAstar
	StringToAlgorithm["dva"] = DynamicViterbiAstar

	StringToAlgorithm["extendgreedy"] = ExtendGreedy
	StringToAlgorithm["eg"] = ExtendGreedy

	StringToAlgorithm["extendsa"] = ExtendSA
	StringToAlgorithm["es"] = ExtendSA

	StringToAlgorithm["extend"] = Extend
	StringToAlgorithm["e"] = Extend

	StringToAlgorithm["greedy"] = GreedyHeuristic
	StringToAlgorithm["g"] = GreedyHeuristic

	StringToAlgorithm["random"] = Random
	StringToAlgorithm["r"] = Random

	StringToAlgorithm["wildtype"] = Wildtype
	StringToAlgorithm["w"] = Wildtype

	StringToAlgorithm["simulatedannealing"] = SimulatedAnnealing
	StringToAlgorithm["s"] = SimulatedAnnealing

}
